from tkinter import *
from tkinter import messagebox
from math import fabs, ceil, radians, cos, sin, floor
import matplotlib.pyplot as plt
import time

root = Tk()
root.geometry('750x1000')
root.resizable(0, 0)
root.title('Лабораторная работа №3')
color_menu = "#DB0000"

canv = Canvas(root, width=750, height=750, bg='white')
canvas = canv
canvas_test = canv
canv.place(x=0, y=250)
center = (375, 200)

def get_intensity(intensity):
    colors = []
    (r1, g1, b1) = canvas.winfo_rgb(line_color)
    (r2, g2, b2) = canvas.winfo_rgb(bg_color)
    dr = (r2 - r1) / intensity
    dg = (g2 - g1) / intensity
    db = (b2 - b1) / intensity
    raccum = 0
    gaccum = 0
    baccum = 0

    for i in range(intensity):
        nr = int(r1 + raccum)
        ng = int(g1 + gaccum)
        nb = int(b1 + baccum)

        raccum += dr
        gaccum += dg
        baccum += db
        colors.append("#%4.4x%4.4x%4.4x" % (nr, ng, nb))
    colors.reverse()
    return colors

line_color = 'black'
bg_color = 'white'
I = 100
grad = get_intensity(I)

def set_linecolor(color):
    global line_color
    line_color = color
    global grad
    grad.clear()
    grad = get_intensity(I)


def clean():
    canvas.delete("all")

def draw_point(x, y, fill = None):
    if (fill == None):
        canv.create_line(x, y, x+1, y, fill = line_color)
    else:
        canv.create_line(x, y, x+1, y, fill = grad[round(fill) - 1])

def draw_cda(xs, ys, xe, ye):
    if (xs == xe and ys == ye):
        draw_point(xs, ys)
        return
    
    lx = xe - xs
    ly = ye - ys

    if abs(lx) > abs(ly):
        l = abs(lx)
    else:
        l = abs(ly)

    dx = lx / l
    dy = ly / l
    x = xs
    y = ys

    for i in range(l):
        draw_point(round(x), round(y))
        x += dx
        y += dy

def sign(a):
    if a > 0:
        return 1
    elif a < 0:
        return -1
    else:
        return 0

def draw_brezenhem_int(xs, ys, xe, ye):
    if (xs == xe and ys == ye):
        draw_point(xs, ys)
        return

    dx = xe - xs
    dy = ye - ys

    SX = sign(dx)
    SY = sign(dy)

    dx = abs(dx)
    dy = abs(dy)

    flag = 0

    if dx < dy:
        dx, dy = dy, dx
        flag = 1

    x = xs
    y = ys
    f = 2*dy - dx
    
    for i in range(dx):
        #print(x, y, f, dx, dy)
        draw_point(x, y)
        if f >= 0:
            if flag:
                x += SX
            else:
                y += SY
            f -= 2*dx
        if f < 0:
            if flag:
                y += SY
            else:
                x += SX
            f += 2*dy

def draw_brezenhem(xs, ys, xe, ye):
    if (xs == xe and ys == ye):
        draw_point(xs, ys)
        return

    dx = xe - xs
    dy = ye - ys

    SX = sign(dx)
    SY = sign(dy)

    dx = abs(dx)
    dy = abs(dy)

    flag = 0

    if dx != 0:
        m = dy / dx
    else:
        m = 0
    if m > 1:
        dx, dy = dy, dx
        m = 1/m
        flag = 1

    x = xs
    y = ys
    f = m - 0.5
    
    for i in range(dx):
        draw_point(x, y)
        if f >= 0:
            if flag:
                x += SX
            else:
                y += SY
            f -= 1
        if f < 0:
            if flag:
                y += SY
            else:
                x += SX
            f += m

def draw_brezenhem_smoth(xs, ys, xe, ye):
    if (xs == xe and ys == ye):
        draw_point(xs, ys)
        return
    
    dx = xe - xs
    dy = ye - ys

    SX = sign(dx)
    SY = sign(dy)

    dx = abs(dx)
    dy = abs(dy)

    flag = 0

    if dx != 0:
        m = dy / dx
    else:
        m = 0
    if m > 1:
        dx, dy = dy, dx
        m = 1/m
        flag = 1

    m *= I
    x = xs
    y = ys
    f = m - (I >> 1)
    
    for i in range(dx):
        #print(x,y)
        draw_point(x, y, fill = f)
        if f >= 0:
            if flag:
                x += SX
            else:
                y += SY
            f -= I
        if f < 0:
            if flag:
                y += SY
            else:
                x += SX
            f += m

def draw_brezenhem_smoth_int(xs, ys, xe, ye):
    if (xs == xe and ys == ye):
        draw_point(xs, ys)
        return
    I = 100
    fill = get_intensity(I)
    
    dx = xe - xs
    dy = ye - ys

    SX = sign(dx)
    SY = sign(dy)

    dx = abs(dx)
    dy = abs(dy)

    flag = 0

    if dy > dx:
        dx, dy = dy, dx
        flag = 1

    x = xs
    y = ys

    dxl = dx * I
    dyl = dy * I
    f = 2*dyl - dxl
    
    for i in range(dx):
        #print(x, y, f)
        draw_point(x, y, fill = (f / dx / 2))
        if f >= 0:
            if flag:
                x += SX
            else:
                y += SY
            f -= 2*(dxl)
        if f < 0:
            if flag:
                y += SY
            else:
                x += SX
            f += 2*(dyl)

def fabs(x):
    if x < 0:
        return x + 1
    else:
        return x

def draw_vu(xs, ys, xe, ye):
    if (xs == xe and ys == ye):
        draw_point(xs, ys)
        return

    dx = xe - xs
    dy = ye - ys
    
    SX = sign(dx)
    SY = sign(dy)

    dx = abs(dx)
    dy = abs(dy)

    flag = 0

    if dx != 0:
        m = dy / dx
    else:
        m = 0
    if m > 1:
        dx, dy = dy, dx
        m = 1/m
        flag = 1

    x = xs
    y = ys
    f = m
    
    for i in range(dx):
        #print(f, I, flag)
        if flag:
            draw_point(x, y, fill = fabs(f)*(I))
            draw_point(x+1, y, fill = (1 - fabs(f))*(I))
        else:
            draw_point(x, y, fill = fabs(f)*(I))
            draw_point(x, y+1, fill = (1 - fabs(f))*(I))
        if f >= 0.5:
            if flag:
                x += SX
            else:
                y += SY
            f -= 1
        if f < 0.5:
            if flag:
                y += SY
            else:
                x += SX
            f += m

cur_draw = draw_cda
funcs = (draw_cda, draw_brezenhem, draw_brezenhem_int, draw_brezenhem_smoth, draw_brezenhem_smoth_int)

def draw():
    try:
        xs, ys, xe, ye = int(fxs.get()), int(fys.get()), int(fxf.get()), int(fyf.get())
        cur_draw(xs, ys, xe, ye)
    except:
        messagebox.showwarning(text = 'Неправильный ввод')

def draw_spectr():
    try:
        angle, R, x, y =  float(fangle.get()), int(fR.get()), int(fcenterX.get()), int(fcenterY.get())

        cur_angle = 0
        while (cur_angle < 360):
            xe = round(x + R*cos(radians(cur_angle)))
            ye = round(y + R*sin(radians(cur_angle)))
            cur_draw(x, y, xe, ye)
            cur_angle += angle
    except:
        messagebox.showwarning(text = 'Неправильный ввод')
        

coords_frame = Frame(root, height=250, width=301)
coords_frame.place(x=0, y=0)

comparison_frame = Frame(root, height=200, width=150)
comparison_frame.place(x=600, y=0)

angle_frame = Frame(root, height=200, width=150)
angle_frame.place(x=300, y=0)

color_frame = Frame(root, height=200, width=150)
color_frame.place(x=450, y=0)

menu_frame = Frame(root, height=200, width=150)
menu_frame.place(x=750, y=0)

def choose_draw(func):
    global cur_draw
    cur_draw = func

# Список Алгоритмов
btn_met1 = Button(coords_frame, text=u"Цифровой дифференциальный анализатор", command=lambda: choose_draw(draw_cda), width=140, height=25)
btn_met1.place(x=3, y=3, width=300, height=20)
btn_met1 = Button(coords_frame, text=u"Брезенхем", command=lambda: choose_draw(draw_brezenhem), width=140, height=25)
btn_met1.place(x=3, y=25, width=300, height=20)
btn_met1 = Button(coords_frame, text=u"Брезенхем (целочисленный)", command=lambda: choose_draw(draw_brezenhem_int), width=140, height=25)
btn_met1.place(x=3, y=45, width=300, height=20)
btn_met1 = Button(coords_frame, text=u"Брезенхем с устранением ступенчатости", command=lambda: choose_draw(draw_brezenhem_smoth), width=140, height=25)
btn_met1.place(x=3, y=65, width=300, height=20)
btn_met1 = Button(coords_frame, text=u"Брезенхем с устранением(целочисленный)", command=lambda: choose_draw(draw_brezenhem_smoth_int), width=140, height=25)
btn_met1.place(x=3, y=85, width=300, height=20)
btn_met1 = Button(coords_frame, text=u"Ву", command=lambda: choose_draw(draw_vu), width=140, height=25)
btn_met1.place(x=3, y=105, width=300, height=20)

lb1 = Label(coords_frame, text='Начало линии:')
lb2 = Label(coords_frame, text='Конец линии:')
lb1.place(x=3, y=135)
lb2.place(x=3, y=175)

lbx1 = Label(coords_frame, text='X:')
lby1 = Label(coords_frame, text='Y:')
lbx2 = Label(coords_frame, text='X:')
lby2 = Label(coords_frame, text='Y:')
lbx1.place(x=90, y=135)
lby1.place(x=90, y=155)
lbx2.place(x=90, y=175)
lby2.place(x=90, y=195)

fxs = Entry(coords_frame, bg="white")
fys = Entry(coords_frame, bg="white")
fxf = Entry(coords_frame, bg="white")
fyf = Entry(coords_frame, bg="white")
fxs.place(x=105, y=135, width=35)
fys.place(x=105, y=155, width=35)
fxf.place(x=105, y=175, width=35)
fyf.place(x=105, y=195, width=35)

btn_draw = Button(coords_frame, text=u"Построить", command=lambda: draw(), width=140, height=25)
btn_draw.place(x=40, y=225, width=70, height=20)

lb_angle = Label(angle_frame, text="Угол поворота\n(в градусах < 360):")
lb_angle.place(x=2, y=2)

fangle = Entry(angle_frame, bg="white")
fangle.place(x=30, y=40, width=25)

lb_R = Label(angle_frame, text="Длина отрезков:")
lb_R.place(x=2, y=60)

fR = Entry(angle_frame, bg="white")
fR.place(x=30, y=80, width=25)

lb_center = Label(angle_frame, text="Координаты центра:")
lb_center.place(x=2, y=100)
lb_centerX = Label(angle_frame, text="X:")
lb_centerX.place(x=2, y=120)
lb_centerY = Label(angle_frame, text="Y:")
lb_centerY.place(x=50, y=120)

fR = Entry(angle_frame, bg="white")
fR.place(x=30, y=80, width=25)
fcenterX = Entry(angle_frame, bg="white")
fcenterX.place(x=2, y=140, width=25)
fcenterY = Entry(angle_frame, bg="white")
fcenterY.place(x=50, y=140, width=25)

btn_viz = Button(angle_frame, text=u"Построение спектра", command=lambda: draw_spectr())
btn_viz.place(x=2, y=160, width=120, height=25)

lb_len = Label(comparison_frame, text="Длина линии\n(по умолчанию 100):")
lb_len.place(x=2, y=2)
len_line = Entry(comparison_frame, bg="white")
len_line.place(x=40, y=40, width=40)
btn_time = Button(comparison_frame, text=u"Сравнение времени\nпостроения", command=lambda: analyze(0))
btn_time.place(x=3, y=70, width=140, height=40)
btn_smoth = Button(comparison_frame, text=u"Сравнение\nступенчатости", command=lambda: analyze(1))
btn_smoth.place(x=3, y=130, width=140, height=40)

# выбор цветов

size = 15
white_line = Button(color_frame, bg="white", activebackground="white",
                    command=lambda: set_linecolor('white'))
white_line.place(x=15, y=30, height=size, width=size)
black_line = Button(color_frame, bg="yellow", activebackground="black",
                    command=lambda: set_linecolor("yellow"))
black_line.place(x=30, y=30, height=size, width=size)
red_line = Button(color_frame, bg="orange", activebackground="orange",
                  command=lambda: set_linecolor("orange"))
red_line.place(x=45, y=30, height=size, width=size)
orange_line = Button(color_frame, bg="red", activebackground="red",
                     command=lambda: set_linecolor("red"))
orange_line.place(x=60, y=30, height=size, width=size)
yellow_line = Button(color_frame, bg="purple", activebackground="purple",
                     command=lambda: set_linecolor("purple"))
yellow_line.place(x=75, y=30, height=size, width=size)
green_line = Button(color_frame, bg="darkblue", activebackground="darkblue",
                    command=lambda: set_linecolor("darkblue"))
green_line.place(x=90, y=30, height=size, width=size)
doger_line = Button(color_frame, bg="darkgreen", activebackground="darkgreen",
                    command=lambda: set_linecolor("darkgreen"))
doger_line.place(x=105, y=30, height=size, width=size)
blue_line = Button(color_frame, bg="black", activebackground="black",
                   command=lambda: set_linecolor("black"))
blue_line.place(x=120, y=30, height=size, width=size)


lb_line = Label(color_frame, text='Цвет линии:')
lb_line.place(x=2, y=5)

btn_clean = Button(color_frame, text=u"Очистить экран", command=clean)
btn_clean.place(x=20, y=80, width=95)

root.mainloop()

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "point.h"
#include <iostream>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    void paintEvent(QPaintEvent *event);
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    std::vector<std::vector<Point>> res;
    Point get_scale(double x_min, double x_max, double y_min, double y_max);
    Point get_axis(double x_min, double x_max, double y_min, double y_max);

    void count_func(double x_start, double x_stop, int x_count, double z_start, double z_stop, int z_count, double (*f)(double , double));
    Point scale_coef;
    Point axis;

    void scale(Point &);

    void rotate_x(double &x, double &y, double &z);
    void rotate_y(double &x, double &y, double &z);
    void rotate_z(double &x, double &y, double &z);

    Point complex_rotate(double x, double y, double z);
    Point scale_rotate(double x, double y, double z);

    bool draw_horizon = false;


    void floating_horizon(double x_start, double x_stop, int x_count, double z_start, double z_stop, int z_count, double (*f)(double , double));

    double x_angle = 0, y_angle = 0, z_angle = 0;

    double (*cur_func)(double, double);

    double xs, xe, zs, ze;
    int z_count;

    int min_x = 0;

    void new_min(double x_start, double x_stop, int x_count, double z_start, double z_stop, int z_count, double (*f)(double , double));


private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_radioButton_clicked();

    void on_radioButton_2_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H

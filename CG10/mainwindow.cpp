#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <vector>
#include <fstream>
#include <QPainter>
#include <float.h>
#include <QTime>

#define WINDOW_HEIGHT 900
#define WINDOW_WIDTH 900
#define OFFSET 40

#define EPS 1e-6

double func(double x, double z)
{
    return sin(x) * cos(z);
}

double func1(double x, double z)
{
    return sin(x) * sin(x) + cos(z) * cos(z) - 1;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    cur_func = func;
}

typedef enum {INVISIBLE = 0, VISIBLE_MAX, VISIBLE_MIN} vstate_t;

void MainWindow::rotate_x(double &x, double &y, double &z)
{
    double tmp_y;
    double tmp_z;
    tmp_y = y * cos(x_angle) - z * sin(x_angle);
    tmp_z = y * sin(x_angle) + z * cos(x_angle);

    y = tmp_y;
    z = tmp_z;
}
void MainWindow::rotate_y(double &x, double &y, double &z)
{
    double tmp_x;
    double tmp_z;
    tmp_x = x * cos(y_angle) - z * sin(y_angle);
    tmp_z = x * sin(y_angle) + z * cos(y_angle);

    x = tmp_x;
    z = tmp_z;
}
//void MainWindow::rotate_z(double &x, double &y, double &z)
//{
//    point tmp;
//    tmp.x = x * cos(angle) - y * sin(angle);
//    tmp.y = x * sin(angle) + y * cos(angle);
//    tmp.z = z;
//    *this = tmp;
//}

Point MainWindow::complex_rotate(double x, double y, double z)
{
    Point res;
    rotate_x(x, y, z);
    rotate_y(x, y, z);

    return Point(x, y);
}

Point MainWindow::scale_rotate(double x, double y, double z)
{
    Point res = complex_rotate(x, y ,z);
    scale(res);
    return res;
}

Point MainWindow::get_scale(double min_x, double max_x, double min_y, double max_y)
{
    Point res;

    if (max_x * min_x < 0)
        res.setX(WINDOW_WIDTH / (max_x - min_x));
    else
        res.setX(max_x > 0 ? WINDOW_WIDTH / max_x : WINDOW_WIDTH / min_x);

    if (max_y * min_y < 0)
        res.setY(WINDOW_HEIGHT / (max_y - min_y));
    else
        res.setY(max_y > 0 ? WINDOW_HEIGHT / max_y : WINDOW_HEIGHT / min_y);

    res.setX(res.getX() < res.getY() ? res.getX() : res.getY());
    res.setY(res.getX());

    return res;
}

Point MainWindow::get_axis(double min_x, double max_x, double min_y, double max_y)
{
    Point res;

    if (max_x * min_x < 0.0)
        res.setX(OFFSET + WINDOW_WIDTH * (-min_x) / (double)(max_x - min_x));
    else
        res.setX(max_x > 0 ? OFFSET : WINDOW_WIDTH + OFFSET);

    if (max_y * min_y < 0.0)
        res.setY(OFFSET + WINDOW_HEIGHT * (1.0 - (-min_y) / (double)(max_y - min_y)));
    else
        res.setY(max_y > 0 ? WINDOW_HEIGHT + OFFSET : OFFSET);

    return res;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
    painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
    painter.drawRect(0, 0, (WINDOW_WIDTH + 2 * OFFSET), (WINDOW_HEIGHT + 2 * OFFSET));

    if (!draw_horizon)
    for (const auto &line : res)
        for (int i = 0; i < line.size() - 1; i+= 2)
        {
            painter.drawLine(line[i].getX() - min_x, line[i].getY(), line[i + 1].getX() - min_x, line[i + 1].getY());
        }


    painter.setPen(QPen(Qt::red, 2, Qt::SolidLine, Qt::FlatCap));
    painter.drawLine(axis.getX(), 0, axis.getX(), (WINDOW_WIDTH + 2 * OFFSET));
    painter.drawLine(0, axis.getY(), (WINDOW_HEIGHT + 2 * OFFSET), axis.getY());
}

void horizon(int xs, int xe, double ys, double ye, std::vector<double> &min, std::vector<double> &max)
{
    if (xs == xe)
    {
        max[xs] = std::max(ys, max[xs]);
        min[xs] = std::min(ys, min[xs]);
        return;
    }

    double m = (ye - ys) / (xe - xs);

    if (xs < xe)
        for (int x = xs; x <= xe; x++)
        {
            double y = (x - xs) * m + ys;

            max[x] = std::max(y, max[x]);
            min[x] = std::min(y, min[x]);
        }
    else
        for (int x = xe; x <= xs; x++)
        {
            double y = (x - xs) * m + ys;

            max[x] = std::max(y, max[x]);
            min[x] = std::min(y, min[x]);
        }
}

void sleepFeature()
{
    QTime end = QTime::currentTime().addMSecs(10);
    while (QTime::currentTime() < end)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 1);
    }
    return;
}

void MainWindow::floating_horizon(double x_start, double x_stop, int x_count, double z_start, double z_stop, int z_count, double (*f)(double , double))
{
    double dx = (x_stop - x_start) / x_count;
    double dz = (z_start - z_stop) / z_count;

    std::vector<double>max(WINDOW_WIDTH + 2 * OFFSET, -DBL_MAX);
    std::vector<double>min(WINDOW_WIDTH + 2 * OFFSET, DBL_MAX);


    std::vector<Point> buf;

    double x_prev, y_prev;
    double z_cur = z_stop;

    for (int i = 0; i < z_count; i++)
    {
        vstate_t state;
        vstate_t prev_state;

        double x_cur = x_start;

        for (int j = 0; j < x_count; j++)
        {
            double y_s = f(x_cur, z_cur);

            Point cur_point = scale_rotate(x_cur, y_s, z_cur);

            double x, y;
            x = cur_point.getX();
            y = cur_point.getY();

            int rounded_x = round(x);

            if (rounded_x > 0 && rounded_x < WINDOW_WIDTH + 2 * OFFSET)
            {
            if (y > max[rounded_x])
            {
                state = VISIBLE_MAX;
            }
            else if (y < min[rounded_x])
            {
                state = VISIBLE_MIN;
            }
            else
                state = INVISIBLE;

            if (j == 0 || (prev_state && state))
            {
                if (j != 0)
                {
                    horizon(round(x_prev), rounded_x, y_prev, y, min, max);
                    buf.push_back(Point(round(x_prev), round(y_prev)));
                    buf.push_back(Point(rounded_x, round(y)));
                }
                else
                {
                    x_prev = x;
                    y_prev = y;
                }
            }
            else if (prev_state || state)
            {
                double y_hor, py_hor;
                if (state == VISIBLE_MAX || prev_state == VISIBLE_MAX)
                    y_hor = max[rounded_x], py_hor = max[round(x_prev)];
                else
                    y_hor = min[rounded_x], py_hor = min[round(x_prev)];

                double dy_hor = y_hor - py_hor;
                double dy_lin = y - y_prev;

                double x_cross, y_cross;

                if (fabs(dy_hor - dy_lin) > EPS)
                    x_cross = x_prev + (x - x_prev) * (y_prev - py_hor) / (dy_hor - (dy_lin));
                else
                    x_cross = x_prev;
                y_cross = (x_cross - x_prev) / (x - x_prev) * dy_hor + py_hor;

                int x_cross_rounded = round(x_cross);
                if (x_cross_rounded > 0)
                {

                    if (prev_state == INVISIBLE)
                    {
                        buf.push_back(Point(x_cross_rounded, round(y_cross)));
                        buf.push_back(Point(rounded_x, round(y)));
                        horizon(x_cross_rounded, rounded_x, y_cross, y, min, max);
                    }
                    else
                    {
                        buf.push_back(Point(round(x_prev), round(y_prev)));
                        buf.push_back(Point(x_cross_rounded, round(y_cross)));
                        horizon(round(x_prev), x_cross_rounded, y_prev, y_cross, min, max);
                    }
                }
            }
            prev_state = state;
            x_prev = x, y_prev = y;
            }


            x_cur += dx;
        }
        if (buf.size() != 0)
            res.push_back(buf);

        //std::cout << buf.size() << "\n";
        //for (auto elem :buf)
        //    std::cout << elem.getX() - axis.getX() << " " << elem.getY() - axis.getY() << " ";
        //std::cout << "\n";

        buf.clear();

        //draw_horizon = true;
        //repaint();
        //sleepFeature();
        //
        //draw_horizon = false;

        z_cur += dz;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::count_func(double x_start, double x_stop, int x_count, double z_start, double z_stop, int z_count, double (*f)(double, double))
{
    double dx = (x_stop - x_start) / x_count;
    double dz = (z_stop - z_start) / z_count;

    double z_cur = z_start;

    double max_y, min_y;
    double x_min = x_start, x_max = x_stop;
    max_y = min_y = f(x_start, z_start);

    for (int i = 0; i < z_count; i++)
    {
        double x_cur = x_start;
        for (int j = 0; j < x_count; j++)
        {
            double y = f(x_cur, z_cur);
            double x_tmp = x_cur;
            double z_tmp = z_cur;

            Point tmp = complex_rotate(x_tmp, y, z_tmp);

            double tmpy = tmp.getY();
            double tmpx = tmp.getX();

            if (tmpy > max_y)
                max_y = tmpy;
            if (tmpy < min_y)
                min_y = tmpy;

            if (tmpx > x_max)
                x_max = tmpx;

            if (tmpx < x_min)
                x_min = tmpx;

            x_cur += dx;
        }

        z_cur += dz;
    }
    //std::cout << x_min << " " << x_max << "\n";
    //std::cout << min_y << " " << max_y << "\n";
    //std::cout << "\n \n \n";

    scale_coef = get_scale(x_min, x_max, min_y, max_y);
    axis = get_axis(x_min, x_max, min_y, max_y);
}

void MainWindow::scale(Point &point)
{
    double x = min_x + axis.getX() + point.getX() * scale_coef.getX();
    double y = axis.getY() - point.getY() * scale_coef.getY();

    point.setX(x);
    point.setY(y);
}

void MainWindow::on_pushButton_clicked()
{
    x_angle = 0;
    y_angle = 0;
    res.clear();
    xs = ui->lineEdit->text().toDouble();
    xe = ui->lineEdit_2->text().toDouble();
    zs = ui->lineEdit_3->text().toDouble();
    ze = ui->lineEdit_4->text().toDouble();

    z_count = ui->lineEdit_8->text().toInt();

    count_func(xs, xe, 100, zs, ze, z_count, cur_func);

    floating_horizon(xs, xe, 100, zs, ze, z_count, cur_func);

    repaint();
}

static inline double toRads(double number)
{
    return number * M_PI / 180.0;
}

void MainWindow::new_min(double x_start, double x_stop, int x_count, double z_start, double z_stop, int z_count, double (*f)(double, double))
{
    double dx = (x_stop - x_start) / x_count;
    double dz = (z_stop - z_start) / z_count;

    double z_cur = z_start;

    double max_x = -DBL_MAX;
    double min_x = DBL_MAX;

    for (int i = 0; i < z_count; i++)
    {
        double x_cur = x_start;
        for (int j = 0; j < x_count; j++)
        {
            double y = f(x_cur, z_cur);
            Point tmp(complex_rotate(x_cur, y, z_cur));

            double x = tmp.getX();

            if (x > max_x)
                max_x = x;
            if (x < min_x)
                min_x = x;

            //std::cout << x << "\n";
            x_cur += dx;
        }
        z_cur += dz;
    }

    if (min_x < x_start)
        this->min_x = (x_start - min_x) * scale_coef.getX();
    else
        this->min_x = (x_stop - max_x) * scale_coef.getX();

}

void MainWindow::on_pushButton_2_clicked()
{
    res.clear();
    x_angle += toRads(ui->lineEdit_5->text().toDouble());
    //count_func(xs, xe, 100, zs, ze, z_count, cur_func);

    floating_horizon(xs, xe, 100, zs, ze, z_count, cur_func);

    repaint();
}

void MainWindow::on_pushButton_3_clicked()
{
    res.clear();
    y_angle += toRads(ui->lineEdit_6->text().toDouble());
    //count_func(xs, xe, 100, zs, ze, z_count, cur_func);

    floating_horizon(xs, xe, 100, zs, ze, z_count, cur_func);

    repaint();
}

void MainWindow::on_pushButton_4_clicked()
{
    res.clear();
    z_angle += toRads(ui->lineEdit_7->text().toDouble());
    repaint();
}

void MainWindow::on_radioButton_clicked()
{
    cur_func = func;
}

void MainWindow::on_radioButton_2_clicked()
{
    cur_func = func1;
}

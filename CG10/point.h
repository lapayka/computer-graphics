#ifndef POINT_H
#define POINT_H


class Point
{
    double x, y;
    public:
        Point();
        Point(double x, double y)
        {
            this->x = x;
            this->y = y;
        }

        double getX() const
        {
            return x;
        }
        double getY() const
        {
            return y;
        }

        void setX(double _x)
        {
            x = _x;
        }

        void setY(double _y)
        {
            y = _y;
        }

        Point operator + (Point point) const {point.x = x + point.x; point.y = y + point.y; return point;}
        Point operator - (Point point) const {point.x = x - point.x; point.y = y - point.y; return point;}
        Point operator * (double num) const {Point buf(*this); buf.x *= num; buf.y *= num; return buf; }

        double operator * (const Point &point) const {return x * point.x + y * point.y; };
        double vector_prod (const Point &point) const {return x * point.y - y * point.x; };

        Point &operator = (const Point &_point) {x = _point.x; y = _point.y; return (*this); };
};

#endif // POINT_H

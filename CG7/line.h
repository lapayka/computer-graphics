#ifndef LINE_H
#define LINE_H

#include "point.h"
class Line
{
private:
    Point first, second;
public:
    Line(Point _first, Point _second)
        : first(_first)
        , second(_second)
    {};

    Point get_fst() const {return first; };
    Point get_scd() const  {return second; };
};

#endif // LINE_H

#include "image.h"

#define EPS 1e-6

Image::Image(int x_start, int x_end, int width, int height, QWidget *parent) :
    QWidget(parent)
  , plast(0, 0)
  , cutter(Point(0,0), Point(0,0))
{
    cutter_color = Qt::red;
    new_line_color = Qt::black;
    line_color =  Qt::green;

    points_count = 0;

    image = new QImage(width, height, QImage::Format_RGB32);
    image->fill(Qt::white);
    setGeometry(x_start, x_end, width, height);

    setMouseTracking(true);
    repaint();
}

void Image::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(0, 0, *image);
    //if (draw_cutter)
    //{
    //    int x1 = cutter.get_left().getX();
    //    int x2 = cutter.get_right().getX();
    //    int y1 = cutter.get_left().getY();
    //    int y2 = cutter.get_right().getY();
    //
    //    int x_min = x1 < x2 ? x1 : x2;
    //    int x_max = x1 + x2 - x_min;
    //
    //    int y_min = y1 < y2 ? y1 : y2;
    //    int y_max = y1 + y2 - y_min;
    //
    //    painter.drawText(x_min, y_min, QString("(%1,%2)").arg(x_min).arg(y_min));
    //    painter.drawText(x_max, y_max, QString("(%1,%2)").arg(x_max).arg(y_max));
    //    painter.drawImage(0, 0, *image);
    //    draw_cutter = false;
    //    update();
    //}
}

int sign(int c)
{
    if (c < 0)
        return -1;
    else if (c > 0)
        return 1;
    else
        c = 0;

    return c;
}

void Image::inputPoint(const Point &point)
{
    if (points_count == 0)
    {
        plast = point;
        points_count++;
        return;
    }
    else
    {
        switch (input_type)
        {
        case DEFAULT_INPUT:
        {
            lines.push_back(Line(plast, point));
            drawLines(lines, line_color);
            //printf("%d %d\n", plast.getX(), plast.getY());
            break;
        }
        case CUTTER_INPUT:
        {

        }
        }
    }
    input_type = DEFAULT_INPUT;
    update();
}

void Image::mousePressEvent(QMouseEvent *event)
{
    inputPoint(Point(event->position().x(), event->position().y()));
}

void Image::clear()
{
    lines.clear();
    cutter = Cutter(Point(0,0), Point(0,0));
    input_type = DEFAULT_INPUT;
    image->fill(Qt::white);
    update();
}

void Image::drawLines(const std::vector<Line> &lines, const QColor &color)
{
    for (const auto &line : lines)
        drawLine(line, color);
    update();
}

void Image::drawCutter()
{
    image->fill(Qt::white);
    drawLines(lines, line_color);

    int x1 = cutter.get_left().getX();
    int x2 = cutter.get_right().getX();
    int y1 = cutter.get_left().getY();
    int y2 = cutter.get_right().getY();

    drawLine(Point(x1, y1), Point(x1, y2), cutter_color);

    drawLine(Point(x1, y1), Point (x2, y1), cutter_color);

    drawLine(Point(x2, y1), Point(x2, y2), cutter_color);

    drawLine(Point(x1, y2), Point(x2, y2), cutter_color);
    //draw_cutter = true;
    //repaint();
}

void Image::drawLine(const Point &pbegin, const Point &pend, const QColor &color)
{
    if (pbegin.getX() == pend.getX() && pbegin.getY() == pend.getY())
        {
            image->setPixelColor(pbegin.getX(), pbegin.getY(), color);
            return;
        }
        Point cur(pbegin);
        int dx, dy;
        bool fl = 0;
        int sx, sy, f;
        dx = pend.getX() - pbegin.getX();
        dy = pend.getY() - pbegin.getY();
        sx = sign(dx);
        sy = sign(dy);
        dx = abs(dx);
        dy = abs(dy);
        if (dy >= dx)
        {
            int tmp = dx;
            dx = dy;
            dy = tmp;
            fl = 1;
        }
        f = 2 * dy - dx;
        int len = dx;
        for (int i = 0; i <= len; i++)
        {
            //printf("**%d %d", cur.getX(), cur.getY());
            image->setPixelColor(cur.getX(), cur.getY(), color);
            if (f >= 0)
            {
                if (fl == 1)
                    cur.setX(cur.getX() + sx);
                else
                    cur.setY(cur.getY() + sy);
                f -= 2 * dx;
            }
            if (f < 0)
            {
                if (fl == 1)
                    cur.setY(cur.getY() + sy);
                else
                    cur.setX(cur.getX() + sx);
            }
            f += 2 * dy;
        }
}


void Image::cut()
{
    int cleft = cutter.get_left().getX();
    int cright = cutter.get_right().getX();
    int cbot = cutter.get_left().getY() < cutter.get_right().getY() ? cutter.get_left().getY() : cutter.get_right().getY();
    int chigh = cutter.get_right().getY() + cutter.get_left().getY() - cbot;

    std::vector<Line> new_lines;

    for (const auto &line :lines)
    {
        Point fst = line.get_fst();
        Point scd = line.get_scd();
        Point buf[2] = {fst, scd};
        int x1 = fst.getX();
        int x2 = scd.getX();
        int y1 = fst.getY();
        int y2 = scd.getY();

        unsigned int S1 = ((x1 < cleft) << 4) + ((x1 > cright) << 3) + ((y1 < cbot) << 2) + ((y1 > chigh));
        unsigned int S2 = ((x2 < cleft) << 4) + ((x2 > cright) << 3) + ((y2 < cbot) << 2) + ((y2 > chigh));

        if (!S1 && !S2)
        {
            new_lines.push_back(line);
            continue;
        }
        if (S1 & S2)
            continue;

        Point R[2];
        Point Q;
        bool flag = true;

        for (int i = 0; i < 2; i++)
        {
            if (S1 == 0)
            {
                R[0] = fst;
                i++;
                Q = scd;
            }
            else
            {
                if (S2 == 0)
                {
                    R[0] = scd;
                    i++;
                    Q = fst;
                }
                else
                    Q = buf[i];
            }
            double m = 1.0;
            double yp;
            if (x1 != x2)
            {
                m = (double)((y2 - y1)) / (x2 - x1);
            }
            if (Q.getX() < cleft)
            {
                yp = m * (cleft - Q.getX()) + Q.getY();
                if (cbot <= yp && yp <= chigh)
                {
                    R[i].setX(cleft);
                    R[i].setY(yp);
                    continue;
                }

            }
            else if (Q.getX() > cright)
            {
                yp = m * (cright - Q.getX()) + Q.getY();
                if (cbot <= yp && yp <= chigh)
                {
                    R[i].setX(cright);
                    R[i].setY(yp);
                    continue;
                }

            }
            double xp;
            if (fabs(m) < EPS)
                continue;
            if (Q.getY() > chigh)
            {
                if (x1 != x2)
                    xp = (chigh - Q.getY()) / m + Q.getX();
                else
                    xp = Q.getX();

                if (cleft <= xp && xp <= cright)
                {
                    R[i].setX(xp);
                    R[i].setY(chigh);
                    continue;
                }
            }
            else if (Q.getY() < cbot)
            {
                if (x1 != x2)
                    xp = (cbot - Q.getY()) / m + Q.getX();
                else
                    xp = Q.getX();

                if (cleft <= xp && xp <= cright)
                {
                    R[i].setX(xp);
                    R[i].setY(cbot);
                    continue;
                }
            }
            flag = false;

        }

        if (flag)
            new_lines.push_back(Line(R[0], R[1]));
    }
    drawLines(new_lines, new_line_color);
}

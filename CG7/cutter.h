#ifndef CUTTER_H
#define CUTTER_H

#include "point.h"

class Cutter
{
private:
    Point left, right;
public:
    Cutter(Point fst, Point scd)
    {
        left = (fst.getX() <  scd.getX()) ? fst : scd;
        right = (fst.getX() >  scd.getX()) ? fst : scd;;
    };

    Point get_left() {return left; };
    Point get_right()  {return right; };
};

#endif // CUTTER_H

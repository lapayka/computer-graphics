#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    image = new Image(0, 0, 600, 600, this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    image->SetInputType(HORIZONTAL_INPUT);
}

void MainWindow::on_pushButton_2_clicked()
{
    image->SetInputType(VERTICAL_INPUT);
}

void MainWindow::on_pushButton_3_clicked()
{
    image->SetInputType(CUTTER_INPUT);
}

void MainWindow::on_pushButton_5_clicked()
{
    image->clear();
}

void MainWindow::on_pushButton_6_clicked()
{
    image->cut();
}

void MainWindow::on_pushButton_4_clicked()
{
    int x = ui->lineEdit->text().toInt();
    int y = ui->lineEdit_2->text().toInt();

    image->inputPoint(Point(x, y));
}

#ifndef IMAGE_H
#define IMAGE_H

#include <QWidget>
#include <QImage>
#include <QPainter>
#include <QMouseEvent>
#include <QPaintEvent>
#include "point.h"
#include <vector>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <QTime>
#include <QCoreApplication>
#include "cutter.h"
#include "line.h"

#define DEFAULT_INPUT 0
#define HORIZONTAL_INPUT 1
#define VERTICAL_INPUT 2
#define CUTTER_INPUT 4

#include <QPainter>
#include <iostream>


class Image : public QWidget
{
    Q_OBJECT
public:
    explicit Image(int x_start, int x_end, int width, int height, QWidget *parent);
    void mousePressEvent(QMouseEvent *event);
    void inputPoint(const Point &point);
    void SetInputType(int type) {input_type = type; };
    void drawLine(const Point &pbegin, const Point &pend, const QColor &color);
    void drawLine(const Line &line, const QColor &color) { drawLine(line.get_fst(), line.get_scd(), color); };
    void drawLines(const std::vector<Line> &lines, const QColor &color);
    void cut();
    void drawCutter();
    void clear();
    void paintEvent(QPaintEvent *event);

private:
    std::vector<Line> lines;
    Cutter cutter;

    int points_count;
    QImage *image;

    QColor cutter_color;
    QColor new_line_color;
    QColor line_color;

    int input_type = DEFAULT_INPUT;
    Point plast;

    bool draw_cutter;

    bool delay;

signals:

};

#endif // IMAGE_H

#include "ellipse.h"

Ellipse::Ellipse()
{

}


Ellipse::Ellipse(Point &src_center, int a, int b)
{
    center = src_center;
    this->a = a;
    this->b = b;
}

void Ellipse::draw_classic(QColor &color)
{
    int x = 0;
    int y;

    int a2 = a*a;
    int b2 = b*b;

    for (int x_gran = a2 / (sqrt(a2 + b2)); x < x_gran; x++)
    {
        y = round(b * sqrt(1 - (x * x) / (double) (a2)));
        points.append_four_points(x, y, center, color);
    }
    for (int y = b2 / (sqrt(a2 + b2)) + 1; y >= 0; y--)
    {
        x = round(a * sqrt(1 - (y * y) / (double) (b2)));
        points.append_four_points(x, y, center, color);
    }
}

void Ellipse::draw_params(QColor &color)
{
    double step = (a > b) ? (1.0 / a) : (1.0 / b);
    //printf("---%d---", b);
    for (double t = 0.0; t < M_PI_2; t += step)
    {

        int x = round(a * cos(t));
        int y = round(b * sin(t));
        //printf("%d %d %f\n", x, y, t);

        points.append_four_points(x, y, center, color);
    }
}

static inline void horisontal_step(int &D, int &x, int bsq)
{
    x++;
    D += bsq * (2 * x + 1);
}
static inline void diagonal_step(int &D, int &x, int &y, int asq, int bsq)
{
    x++;
    y--;
    D += (2 * x + 1) * bsq + (1 - 2 * y) * asq;
}
static inline void vertical_step(int &D, int &y, int asq)
{
    y--;
    D += asq * (1 - 2 * y);
}

void Ellipse::draw_brezenhem(QColor &color)
{
    int x = 0;
    int y = b;
    int a2 = a * a;
    int b2 = b * b;

    int D = b2 - a2 * (2 * b - 1);

    int D1, D2;
    while (y >= 0)
    {
        points.append_four_points(x, y, center, color);
        if (D < 0)
        {
           D1 = 2 * D + a2 * (2 * y - 1);
           if (D1 <= 0)
               horisontal_step(D, x, b2);
           else
               diagonal_step(D, x, y, a2, b2);
        }
        else if (D == 0)
        {
            diagonal_step(D, x, y, a2, b2);
        }
        else
        {
            D2 = 2 * D - b2 * (2 * x + 1);
            if (D2 <= 0)
                diagonal_step(D, x, y, a2, b2);
            else
                vertical_step(D, y, a2);
        }
    }
}


void Ellipse::draw_middle_pt(QColor &color)
{
    int a2 = a*a;
    int b2 = b*b;

    int x_gran = a2 / (sqrt(a2 + b2));
    //printf("%d\n", x_gran);
    int f = b2 - a2 * b + a2 / 4;
    int y = b;
    int x = 0;

    for (; x < x_gran;)
    {
        points.append_four_points(x, y, center, color);
        x++;
        if (f <= 0)
            f += b2 * ((x << 1) + 1);
        else
        {
            y--;
            f += b2 * ((x << 1) + 1) - a2 * (y << 1);
        }
    }
    f += 0.75 * (a2 - b2) - (b2 * x + y * a2);
    for (; y >= 0;)
    {
        points.append_four_points(x, y, center, color);
        y--;
        if (f >= 0)
        {
            f -= a2 * ((y << 1) - 1);
        }
        else
        {
            x++;
            f += -a2 * ((y << 1) - 1) + b2 * (x << 1);
        }
    }
}

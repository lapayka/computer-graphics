#ifndef POINTS_H
#define POINTS_H

#include "point.h"
#include <stdio.h>
#include <iostream>
#include <vector>

class Points
{
    std::vector<Point> array;
    public:
        Points();
        void append(Point point);
        void append_four_points(Point point, Point transfer);
        void append_four_points(int x, int y, Point &transfer);
        void append_four_points(int x, int y, Point &transfer, QColor &color);
        int len()
        {
            return array.size();
        }
        Point &operator[](int i)
        {
            return array[i];
        }

        void clear()
        {
            array.clear();
        }

};

#endif // POINTS_H

#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <math.h>
#include "points.h"

class Ellipse
{
    public:
        Ellipse();
        Ellipse(Point &src_center, int a, int b);

    private:
        int a, b;
        Point center = {0, 0};

        Points points;
    public:
        void set_a(int radius)
        {
            a = radius;
        }
        void set_b(int radius)
        {
            b = radius;
        }
        void set_center(Point &cent)
        {
            center = cent;
        }
        void clear()
        {
            points.clear();
        }

        void draw_classic(QColor &color);
        void draw_params(QColor &color);
        void draw_brezenhem(QColor &color);
        void draw_middle_pt(QColor &color);


        Point& get_center()
        {
            return center;
        }
        int get_a()
        {
            return a;
        }
        int get_b()
        {
            return b;
        }
        Points& get_Points()
        {
            return points;
        }
    };

#endif // ELLIPSE_H

#include "points.h"

Points::Points()
{

}


void Points::append_four_points(int x, int y, Point &transfer)
{
    Point tmp(x, y);

    tmp.transfer(transfer);

    array.push_back(tmp);

    tmp.mirrorX(transfer.getY());
    array.push_back(tmp);

    tmp.mirrorY(transfer.getX());
    array.push_back(tmp);

    tmp.mirrorX(transfer.getY());
    array.push_back(tmp);
}

void Points::append_four_points(int x, int y, Point &transfer, QColor &color)
{
    Point tmp(x, y, color);

    tmp.transfer(transfer);
    //printf("%d %d\n", tmp.getX(), tmp.getY());
    array.push_back(tmp);

    tmp.mirrorX(transfer.getY());
    array.push_back(tmp);
    //printf("%d %d\n", tmp.getX(), tmp.getY());

    tmp.mirrorY(transfer.getX());
    array.push_back(tmp);
    //printf("%d %d\n", tmp.getX(), tmp.getY());

    tmp.mirrorX(transfer.getY());
    array.push_back(tmp);
    //printf("%d %d\n", tmp.getX(), tmp.getY());
}

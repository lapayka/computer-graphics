#include "circle.h"

Circle::Circle()
{

}

Circle::Circle(Point &src_center, int src_R)
{
    R = src_R;
    center = src_center;
}

Circle::Circle(int x, int y, int src_R)
{
    Point src_center(x, y);
    center = src_center;
    R = src_R;
}
void Circle::draw_classic(QColor &color)
{
    int Rdiv2 = R / sqrt(2.0);
    int y;
    for (int x = 0; x <= Rdiv2; x++)
    {
        y = round(sqrt(R*R - x*x));

        points.append_four_points(x, y, center, color);
        points.append_four_points(y, x, center, color);
    }
}

void Circle::draw_params(QColor &color)
{
    double step = 1.0 / R;

    for (double t = 0.0; t < M_PI_4; t += step)
    {
        int x = round(R * cos(t));
        int y = round(R * sin(t));

        points.append_four_points(x, y, center, color);
        points.append_four_points(y, x, center, color);
    }
}

static inline void horisontal_step(int &D, int &x)
{
    x++;
    D += (2 * x + 1);
}
static inline void diagonal_step(int &D, int &x, int &y)
{
    x++;
    y--;
    D += (2 * x + 1) + (1 - 2 * y);
}
static inline void vertical_step(int &D, int &y)
{
    y--;
    D += (1 - 2 * y);
}

void Circle::draw_brezenhem(QColor &color)
{
    int x = 0;
    int y = R;
    int D = 2 * (1 - R);
    int Rdiv2 = R / sqrt(2.0);

    while (y >= Rdiv2)
    {
        //printf("%d %d\n", x, y);
        points.append_four_points(x, y, center, color);
        points.append_four_points(y, x, center, color);
        if (D < 0)
        {
            int D1 = 2 * (D + y) - 1;
            if (D1 < 0)
            {
                horisontal_step(D, x);
            }
            else
            {
                diagonal_step(D, x, y);
            }
        }
        else if (D == 0)
        {
            diagonal_step(D, x, y);
        }
        else
        {
            int D2 = 2 * (D - x) - 1;
            if (D2 < 0)
            {
                diagonal_step(D, x, y);
            }
            else
            {
                vertical_step(D, y);
            }
        }
    }
}

void Circle::draw_middle_pt(QColor &color)
{
    int Rdiv2 = R / sqrt(2.0);

    int f = 1-R;//R * R - R * R * R + R * R / 4;
    int y = R;
    int x = 0;

    for (; x <= Rdiv2;)
    {
        points.append_four_points(x, y, center, color);
        points.append_four_points(y, x, center, color);
        x++;
        if (f < 0)
            f += (x << 1) + 1;
        else
        {
            y--;
            f += (x << 1) + 1 - (y << 1); 
        }
    }
}

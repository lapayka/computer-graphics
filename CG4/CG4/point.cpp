#include "point.h"

Point::Point()
{
}

Point::Point(int x, int y)
{
    this->x = x;
    this->y = y;
}

Point::Point(int x, int y, QColor &color)
{
    this->x = x;
    this->y = y;
    this->color = color;
}

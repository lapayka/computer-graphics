#ifndef CIRCLE_H
#define CIRCLE_H

#include <math.h>
#include "point.h"
#include "points.h"


class Circle
{
    public:
        Circle();
        Circle(Point &src_center, int src_R);
        Circle(int x, int y, int src_R);

    private:
        int R;
        Point center = {0, 0};

        Points points;
    public:
        void set_R(int radius)
        {
            R = radius;
        }
        void set_center(Point &cent)
        {
            center = cent;
        }
        void clear()
        {
            points.clear();
        }

        void draw_classic(QColor &color);
        void draw_params(QColor &color);
        void draw_brezenhem(QColor &color);
        void draw_middle_pt(QColor &color);


        Point& get_center()
        {
            return center;
        }
        int getR()
        {
            return R;
        }
        Points& get_Points()
        {
            return points;
        }
};

#endif // CIRCLE_H

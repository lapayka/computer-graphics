#ifndef POINT_H
#define POINT_H

#include <QColor>

class Point
{
    public:
        Point();
        Point(int x, int y);
        Point(int x, int y, QColor &color);
        void setX(int setX)
        {
            x = setX;
        }
        void setY(int setY)
        {
            x = setY;
        }
        void mirrorX()
        {
            x = -x;
        }
        void mirrorY()
        {
            y = -y;
        }
        void mirrorX(int XAxis)
        {
            //printf("%d %d\n", XAxis, y);
            y = (XAxis << 1) - y;
        }
        void mirrorY(int YAxis)
        {
            //printf("%d %d\n", YAxis, x);
            x = (YAxis << 1) - x;
        }
        int getX()
        {
            return x;
        }
        int getY()
        {
            return y;
        }
        QColor &get_color()
        {
            return color;
        }
        void transfer(Point center)
        {
            x += center.getX();
            y += center.getY();
        }
    private:
        int x;
        int y;
        QColor color;
};

#endif // POINT_H

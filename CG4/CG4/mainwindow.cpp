#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <time.h>
#include <stdio.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this); 
}

void MainWindow::draw_point(QPainter &painter, Point &point)
{
    painter.setPen(QPen(point.get_color(), 1, Qt::SolidLine, Qt::FlatCap));
    painter.drawPoint(START_X + point.getX(), START_Y + point.getY());
}

void MainWindow::draw_points(QPainter &painter, Points &points)
{
    for (int i = 0; i < points.len(); i++)
        draw_point(painter, points[i]);
}

void MainWindow::draw_circle(QPainter &painter, Circle &circle)
{
    draw_points(painter, circle.get_Points());
}

void MainWindow::draw_ellipse(QPainter &painter, Ellipse &ellipse)
{
    draw_points(painter, ellipse.get_Points());
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
    painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
    painter.drawRect(START_X, START_Y, START_X + WINDOW_WIDTH, START_Y + WINDOW_HEIGHT);
    painter.setBrush(Qt::NoBrush);
    for (int i = 0; i < lib_circles.size(); i++)
    {
        painter.setPen(QPen(lib_circles[i].color, 1, Qt::SolidLine, Qt::FlatCap));
        QPoint center(START_X + lib_circles[i].center.getX(), START_Y + lib_circles[i].center.getY());
        painter.drawEllipse(center, lib_circles[i].a, lib_circles[i].b);
    }
    draw_circle(painter, circle);
    draw_ellipse(painter, ellipse);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    ui->Circle_Err_L->setText("");
    int x, y;
    int R;

    bool flag;

    x = ui->CircleX_E->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L->setText("Неправильный ввод");
        return;
    }
    y = ui->CircleY_E->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L->setText("Неправильный ввод");
        return;
    }
    R = ui->CircleR_E->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L->setText("Неправильный ввод");
        return;
    }

    Point center(x, y);
    if (circle_lib)
    {
        Circle_lib lib_circle;
        lib_circle.center = center;
        lib_circle.a = R;
        lib_circle.b = R;
        lib_circle.color = color;
        lib_circles.push_back(lib_circle);
    }
    else
    {
        circle.set_center(center);
        circle.set_R(R);
        (circle.*draw_circle_func)(color);
    }

    repaint();

    //circle.clear();
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->Ellipse_Err_L->setText("");
    int x, y;
    int a, b;

    bool flag;

    x = ui->EllipseX_E->text().toInt(&flag);
    if (!flag)
    {
        ui->Ellipse_Err_L->setText("Неправильный ввод");
        return;
    }
    y = ui->EllipseY_E->text().toInt(&flag);
    if (!flag)
    {
        ui->Ellipse_Err_L->setText("Неправильный ввод");
        return;
    }
    a = ui->EllipseA_E->text().toInt(&flag);
    if (!flag)
    {
        ui->Ellipse_Err_L->setText("Неправильный ввод");
        return;
    }
    b = ui->EllipseB_E->text().toInt(&flag);
    //printf("///%d//", b);
    if (!flag)
    {
        ui->Ellipse_Err_L->setText("Неправильный ввод");
        return;
    }

    Point center(x, y);
    if (ellipse_lib)
    {
        Circle_lib lib_circle;
        lib_circle.center = center;
        lib_circle.a = a;
        lib_circle.b = b;
        lib_circle.color = color;
        lib_circles.push_back(lib_circle);
    }
    else
    {
        ellipse.set_center(center);
        ellipse.set_a(a);
        ellipse.set_b(b);
        (ellipse.*draw_ellipse_func)(color);
    }

    repaint();

    //ellipse.clear();
}


void MainWindow::on_pushButton_4_clicked()
{
    ui->Circle_Err_L_2->setText("");
    int x, y;
    int R;

    bool flag;

    x = ui->CircleX_E_2->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_2->setText("Неправильный ввод");
        return;
    }
    y = ui->CircleY_E_2->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_2->setText("Неправильный ввод");
        return;
    }
    R = ui->CircleR_E_2->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_2->setText("Неправильный ввод");
        return;
    }

    int step = ui->CircleR_E_3->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_2->setText("Неправильный ввод");
        return;
    }

    int n = ui->CircleR_E_4->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_2->setText("Неправильный ввод");
        return;
    }

    for (int i = 0; i < n; i++, R += step)
    {
        Point center(x, y);
        if (circle_lib)
        {
            circle.set_center(center);
            circle.set_R(R);
            circle.draw_classic(color);
        }
        else
        {
            circle.set_center(center);
            circle.set_R(R);
            (circle.*draw_circle_func)(color);
        }
    }

    repaint();

    //circle.clear();
}




void MainWindow::on_pushButton_5_clicked()
{
    ui->Circle_Err_L_3->setText("");
    int x, y;
    int a, b;

    bool flag;

    x = ui->EllipseX_E_2->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_3->setText("Неправильный ввод");
        return;
    }
    y = ui->EllipseY_E_2->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_3->setText("Неправильный ввод");
        return;
    }
    a = ui->EllipseA_E_2->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_3->setText("Неправильный ввод");
        return;
    }
    b = ui->EllipseB_E_2->text().toInt(&flag);
    //printf("///%d//", b);
    if (!flag)
    {
        ui->Circle_Err_L_3->setText("Неправильный ввод");
        return;
    }

    int step = ui->EllipseA_E_3->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_3->setText("Неправильный ввод");
        return;
    }

    int n = ui->EllipseB_E_3->text().toInt(&flag);
    if (!flag)
    {
        ui->Circle_Err_L_3->setText("Неправильный ввод");
        return;
    }

    double e = sqrt(1 - ((double)b * b) / (a * a));
    for (int i = 0; i < n; i++, a += step)
    {
        b = sqrt(a*a - e*e*a*a);
        Point center(x, y);
        if (ellipse_lib)
        {
            ellipse.set_center(center);
            ellipse.set_a(a);
            ellipse.set_b(b);
            ellipse.draw_classic(color);
        }
        else
        {
            ellipse.set_center(center);
            ellipse.set_a(a);
            ellipse.set_b(b);
            (ellipse.*draw_ellipse_func)(color);
        }
    }
    repaint();
}


void MainWindow::on_Circle_Lib_clicked()
{
    circle_lib = true;
}

void MainWindow::on_Circle_classic_clicked()
{
    draw_circle_func = &Circle::draw_classic;
    circle_lib = false;
}

void MainWindow::on_Circle_param_clicked()
{
    draw_circle_func = &Circle::draw_params;
    circle_lib = false;
}

void MainWindow::on_Circle_brezenhem_clicked()
{
    draw_circle_func = &Circle::draw_brezenhem;
    circle_lib = false;
}

void MainWindow::on_Circle_midle_pt_clicked()
{
    draw_circle_func = &Circle::draw_middle_pt;
    circle_lib = false;
}

void MainWindow::on_White_clicked()
{
    color = Qt::white;
}

void MainWindow::on_Yellow_clicked()
{
    color = Qt::yellow;
}

void MainWindow::on_Black_clicked()
{
    color = Qt::black;
}


void MainWindow::on_Red_clicked()
{
    color = Qt::red;
}


void MainWindow::on_Blue_clicked()
{
    color = Qt::blue;
}

void MainWindow::on_Green_clicked()
{
    color = Qt::green;
}

void MainWindow::on_Ellipse_Lib_clicked()
{
    ellipse_lib = true;
}

void MainWindow::on_Ellipse_classic_clicked()
{
    draw_ellipse_func = &Ellipse::draw_classic;
    ellipse_lib = false;
}

void MainWindow::on_Ellipse_param_clicked()
{
    draw_ellipse_func = &Ellipse::draw_params;
    ellipse_lib = false;
}

void MainWindow::on_Ellipsebrezenhem_clicked()
{
    draw_ellipse_func = &Ellipse::draw_brezenhem;
    ellipse_lib = false;
}

void MainWindow::on_Ellipse_midle_pt_clicked()
{
    draw_ellipse_func = &Ellipse::draw_middle_pt;
    ellipse_lib = false;
}

void MainWindow::on_pushButton_6_clicked()
{
    //time_t start, stop;
    //FILE *file;
    //
    //setbuf(stdout, NULL);
    //
    //void (Circle::*(funcs[4]))(QColor &);
    //funcs[0] = &Circle::draw_classic;
    //funcs[1] = &Circle::draw_params;
    //funcs[2] = &Circle::draw_brezenhem;
    //funcs[3] = &Circle::draw_middle_pt;
    //
    //file = fopen("CirleTime.txt", "w");
    //
    //for (int R = 1; R < 200; R++)
    //    fprintf(file, "%d ", R);
    //
    //fprintf(file, "\n");
    //QColor color = Qt::black;
    //
    //for (int j = 0; j < 4; j++)
    //{
    //    for (int R = 1; R < 200; R++)
    //    {
    //        Circle tmp(0, 0, R);
    //        start = clock();
    //        for (int i = 0; i < 10000; i++)
    //        {
    //            (tmp.*(funcs[j]))(color);
    //            //printf("lil %d\n", i);
    //            tmp.clear();
    //            //printf("lil %d\n", i);
    //        }
    //        stop = clock();
    //
    //        fprintf(file, "%lf ", (double)(stop - start) / CLOCKS_PER_SEC / 100);
    //    }
    //    fprintf(file, "\n");
    //}
    //fclose(file);

    system("D:\\python3.9\\python.exe CircleTime.py");
}

void MainWindow::on_pushButton_7_clicked()
{
    //time_t start, stop;
    //FILE *file;
    //
    //setbuf(stdout, NULL);
    //
    //void (Ellipse::*(funcs[4]))(QColor &);
    //funcs[0] = &Ellipse::draw_classic;
    //funcs[1] = &Ellipse::draw_params;
    //funcs[2] = &Ellipse::draw_brezenhem;
    //funcs[3] = &Ellipse::draw_middle_pt;
    //
    //file = fopen("EllipseTime.txt", "w");
    //
    //for (int R = 1; R < 200; R++)
    //    fprintf(file, "%d ", R);
    //
    //fprintf(file, "\n");
    //QColor color = Qt::black;
    //Point center(0, 0);
    //
    //double e = 0.8;
    //
    //for (int j = 0; j < 4; j++)
    //{
    //    for (int a = 1; a < 200; a++)
    //    {
    //        int b = sqrt(a*a - e*e*a*a);
    //        Ellipse tmp(center, a, b);
    //        start = clock();
    //        for (int i = 0; i < 10000; i++)
    //        {
    //            (tmp.*(funcs[j]))(color);
    //            //printf("lil %d\n", i);
    //            tmp.clear();
    //            //printf("lil %d\n", i);
    //        }
    //        stop = clock();
    //
    //        fprintf(file, "%lf ", (double)(stop - start) / CLOCKS_PER_SEC / 100);
    //    }
    //    fprintf(file, "\n");
    //}
    //fclose(file);

    system("D:\\python3.9\\python.exe EllipseTime.py");
}

void MainWindow::on_pushButton_3_clicked()
{
    circle.clear();
    ellipse.clear();
    lib_circles.clear();

    repaint();
}

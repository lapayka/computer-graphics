#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include "circle.h"
#include "ellipse.h"
#include <QMainWindow>
#include <QPainter>
#include <QColor>

#define WINDOW_HEIGHT 940
#define WINDOW_WIDTH 500
#define START_X 600
#define START_Y 1

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class Circle_lib
{
    public:
        Circle_lib()
        {
            //center(0,0);
        };
        int a, b;
        Point center = {0, 0};
        QColor color;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    void paintEvent(QPaintEvent *event);
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_Circle_Lib_clicked();

    void on_Circle_classic_clicked();

    void on_Circle_param_clicked();

    void on_Circle_brezenhem_clicked();

    void on_Circle_midle_pt_clicked();

    void on_White_clicked();

    void on_Yellow_clicked();

    void on_Black_clicked();

    //void on_Orange_clicked();

    void on_Red_clicked();

    //void on_Purple_clicked();

    void on_Blue_clicked();

    void on_Green_clicked();

    void on_pushButton_2_clicked();

    void on_Ellipse_Lib_clicked();

    void on_Ellipse_classic_clicked();

    void on_Ellipse_param_clicked();

    void on_Ellipsebrezenhem_clicked();

    void on_Ellipse_midle_pt_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_3_clicked();

private:
    void draw_point(QPainter &painter, Point &point);
    void draw_points(QPainter &painter, Points &points);
    void draw_circle(QPainter &painter, Circle &circle);
    void draw_ellipse(QPainter &painter, Ellipse &ellipse);

    Ui::MainWindow *ui;

    std::vector<Circle_lib> lib_circles;

    QColor color = Qt::black;
    Circle circle;
    void (Circle::*draw_circle_func)(QColor &color);
    bool circle_lib = true;
    bool is_circle = false;

    Ellipse ellipse;
    void (Ellipse::*draw_ellipse_func)(QColor &color);
    bool ellipse_lib = true;
    bool is_ellipse = false;
};
#endif // MAINWINDOW_H

import matplotlib.pyplot as plt


file = open("EllipseTime.txt", 'r')

times = [list(map(float, line.split())) for line in file]

file.close()

R = times[0]
classic = times[1]
params = times[2]
brezenhem = times[3]
middle_pt = times[4]

plt.plot(R, classic, label = 'Каноническое уравнение')
plt.plot(R, params, label = 'Параметрическое уравнение')
plt.plot(R, brezenhem, label = 'Брезенхем')
plt.plot(R, middle_pt, label = 'Средняя точка')
plt.legend()

plt.xlabel("Большая полуось")
plt.ylabel("Время ввыполнения (в секундах)")

plt.show()

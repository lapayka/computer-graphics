#ifndef POINT_H
#define POINT_H


class Point
{
    int x, y;
    public:
        Point();
        Point(int x, int y)
        {
            this->x = x;
            this->y = y;
        }

        int getX() const
        {
            return x;
        }
        int getY() const
        {
            return y;
        }

        void setX(int _x)
        {
            x = _x;
        }

        void setY(int _y)
        {
            y = _y;
        }

};

#endif // POINT_H

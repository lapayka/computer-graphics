#include "figure.h"
#include <windows.h>

Figure::Figure()
{
    end_of_input = true;
}

void Figure::clear()
{
    point_array.clear();
    edge_list.clear();

    end_of_input = true;
}

void Figure::add_point(Point &point)
{
    point_array.push_back(point);


    if (end_of_input)
    {
        start_of_figure = point_array.size() - 1;
        end_of_input = false;
        return;
    }

    Edge edge(point_array.size() - 1, point_array.size() - 2);
    edge_list.push_back(edge);
}

void Figure::end_adding()
{
    Edge edge(point_array.size() - 1, start_of_figure);
    edge_list.push_back(edge);
    end_of_input = true;
}

int max_x(std::vector<Point> &array)
{
    int max_x = array[0].getX();
    for (int i = 1; i < array.size(); i++)
        if (array[i].getX() > max_x)
            max_x = array[i].getX();

    return max_x;
}

int max_y(std::vector<Point> &array)
{
    int max_y = array[0].getY();
    for (int i = 1; i < array.size(); i++)
        if (array[i].getY() > max_y)
            max_y = array[i].getY();

    return max_y;
}

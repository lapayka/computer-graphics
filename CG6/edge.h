#ifndef EDGE_H
#define EDGE_H


class Edge
{
    int A, B;
    public:
        Edge();
        Edge(int a, int b);

        int getA()
        {
            return A;
        }
        int getB()
        {
            return B;
        }
};

#endif // EDGE_H

#include "image.h"

Image::Image(int x_start, int x_end, int width, int height, QWidget *parent) :
    QWidget(parent)
  , plast(0, 0)
{
    pen_color = Qt::red;
    brush_color = Qt::green;
    figure = Figure();
    image = new QImage(width, height, QImage::Format_RGB32);
    bg_color = Qt::white;
    image->fill(bg_color);
    setGeometry(x_start, x_end, width, height);

    setMouseTracking(true);
}

void Image::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(0, 0, *image);
}

int sign(int c)
{
    if (c < 0)
        return -1;
    else if (c > 0)
        return 1;
    else
        c = 0;

    return c;
}

void Image::mousePressEvent(QMouseEvent *event)
{
    printf("%d\n", input_type);
    switch (input_type)
    {
    case DEFAULT_INPUT:
    {
        plast = Point((int)(event->position().x()), (int)(event->position().y()));
        printf("%d %d\n", plast.getX(), plast.getY());
        break;
    }
    case HORIZONTAL_INPUT:
    {
        plast = Point((int)(event->position().x()), plast.getY());
        break;
    }
    case VERTICAL_INPUT:
    {
        plast = Point(plast.getX(), (int)(event->position().y()));
        break;
    }
    case SEED_INPUT:
    {
        Point seed((int)(event->position().x()), (int)(event->position().y()));
        fill(seed, delay);
        input_type = DEFAULT_INPUT;
        drawFigure();
        return;
    }
    }
    repaint();
    input_type = DEFAULT_INPUT;
    figure.add_point(plast);
    drawFigure();
}

void Image::clear()
{
    figure.clear();
    image->fill(bg_color);
    update();
}

void Image::drawFigure()
{
    std::vector<Point> &points = figure.getPoints();
    std::vector<Edge> &edges = figure.getEdges();

    for (int i = 0; i < edges.size(); i++)
    {
        drawLine(Point(points[edges[i].getA()].getX(), points[edges[i].getA()].getY()), Point(points[edges[i].getB()].getX(), points[edges[i].getB()].getY()));
    }
    update();
}

void Image::drawLine(const Point &pbegin, const Point &pend)
{
    if (pbegin.getX() == pend.getX() && pbegin.getY() == pend.getY())
        {
            image->setPixelColor(pbegin.getX(), pbegin.getY(), pen_color);
            return;
        }
        Point cur(pbegin);
        int dx, dy;
        bool fl = 0;
        int sx, sy, f;
        dx = pend.getX() - pbegin.getX();
        dy = pend.getY() - pbegin.getY();
        sx = sign(dx);
        sy = sign(dy);
        dx = abs(dx);
        dy = abs(dy);
        if (dy >= dx)
        {
            int tmp = dx;
            dx = dy;
            dy = tmp;
            fl = 1;
        }
        f = 2 * dy - dx;
        int len = dx;
        for (int i = 0; i <= len; i++)
        {
            //printf("**%d %d", cur.getX(), cur.getY());
            image->setPixelColor(cur.getX(), cur.getY(), pen_color);
            if (f >= 0)
            {
                if (fl == 1)
                    cur.setX(cur.getX() + sx);
                else
                    cur.setY(cur.getY() + sy);
                f -= 2 * dx;
            }
            if (f < 0)
            {
                if (fl == 1)
                    cur.setY(cur.getY() + sy);
                else
                    cur.setX(cur.getX() + sx);
            }
            f += 2 * dy;
        }
}

void sleepFeature()
{
    QTime end = QTime::currentTime().addMSecs(10);
    while (QTime::currentTime() < end)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 1);
    }
    return;
}

void Image::fill(const Point &_seed, bool delay)
{
    std::stack<Point> stack;
    stack.push(_seed);

    while (!stack.empty())
    {
        Point seed = stack.top();
        stack.pop();
        //printf("from stack %d %d\n", seed.getX(), seed.getY());
        if (seed.getY() <= 0 || seed.getY() >= this->geometry().height())
            continue;

        image->setPixelColor(seed.getX(), seed.getY(), brush_color);

        int tmp, x_right, x_left;

        for (tmp = seed.getX(); tmp < this->geometry().width() - 1 && image->pixelColor(tmp, seed.getY()) != pen_color; tmp++)
            image->setPixelColor(tmp, seed.getY(), brush_color);
        x_right = tmp - 1;

        for (tmp = seed.getX(); tmp > 1 && image->pixelColor(tmp, seed.getY()) != pen_color; tmp--)
            image->setPixelColor(tmp, seed.getY(), brush_color);
        x_left = tmp + 1;

        //printf("geometry %d %d\n", x_left, x_right);

        seed.setY(seed.getY() + 1);

        int i = 0;
        do
        {
            for (int tmp = x_left; tmp <= x_right;)
            {
                bool flag = false;
                while (image->pixelColor(tmp, seed.getY()) != brush_color &&
                       image->pixelColor(tmp, seed.getY()) != pen_color &&
                       tmp <= x_right)
                {
                    flag = true;
                    tmp++;
                }
                if (flag)
                {
                    stack.push(Point(tmp - 1, seed.getY()));
                }

                while ((image->pixelColor(tmp, seed.getY()) == brush_color ||
                        image->pixelColor(tmp, seed.getY()) == pen_color) &&
                        tmp <= x_right)
                    tmp++;
            }
            i++;
            seed.setY(seed.getY() - 2);
        }
        while (i != 2);

        if (delay)
        {
            update();
            sleepFeature();
        }

    }
    update();
}

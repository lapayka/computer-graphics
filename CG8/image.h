#ifndef IMAGE_H
#define IMAGE_H

#include <QWidget>
#include <QImage>
#include <QPainter>
#include <QMouseEvent>
#include <QPaintEvent>
#include "figure.h"
#include <stack>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <QTime>
#include <QCoreApplication>

#define LINE_INPUT 16
#define DEFAULT_INPUT 0
#define HORIZONTAL_INPUT 1
#define VERTICAL_INPUT 2
#define SEED_INPUT 4
#define CUTTER_INPUT 8

#include <QPainter>
#include <iostream>
#include "line.h"

class Image : public QWidget
{
    Q_OBJECT
public:
    explicit Image(int x_start, int x_end, int width, int height, QWidget *parent);
    void mousePressEvent(QMouseEvent *event);
    bool EndFigureInput(){bool res = cutter.end_adding(); drawFigure(); input_type = LINE_INPUT; return res; };
    void SetInputType(int type) {input_type = type; };
    void SetInputStyle(int type) {input_style = type; };
    void drawLine(const Point &pbegin, const Point &pend, const QColor &color);
    void drawLine(const Line &line, const QColor &color) {drawLine(line.get_fst(), line.get_scd(), color); };
    void drawFigure();
    void clear();
    void paintEvent(QPaintEvent *event);
    void inputPoint(const Point &point);
    void inputRectPoint(const Point &point);
    void drawLines(const std::vector<Line> &lines, const QColor &color);
    void cut();


private:
    QImage *image;
    Figure cutter;
    std::vector<Line> lines;

    QColor cutter_color;
    QColor new_line_color;
    QColor line_color;

    int input_type = LINE_INPUT;
    int input_style = DEFAULT_INPUT;

    Point plast;
    int points_count = 0;


signals:

};

#endif // IMAGE_H

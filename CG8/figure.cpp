#include "figure.h"
#include <windows.h>

Figure::Figure()
{
    end_of_input = true;
}

void Figure::clear()
{
    point_array.clear();
    edge_list.clear();

    end_of_input = true;
}

void Figure::add_point(const Point &point)
{
    point_array.push_back(point);


    if (end_of_input)
    {
        start_of_figure = point_array.size() - 1;
        end_of_input = false;
        return;
    }

    Edge edge(point_array.size() - 2, point_array.size() - 1);
    edge_list.push_back(edge);
}

bool Figure::end_adding()
{
    Edge edge(point_array.size() - 1, start_of_figure);
    edge_list.push_back(edge);
    end_of_input = true;

    count_normals();
    return is_convex();
}

int max_x(std::vector<Point> &array)
{
    int max_x = array[0].getX();
    for (int i = 1; i < array.size(); i++)
        if (array[i].getX() > max_x)
            max_x = array[i].getX();

    return max_x;
}

int max_y(std::vector<Point> &array)
{
    int max_y = array[0].getY();
    for (int i = 1; i < array.size(); i++)
        if (array[i].getY() > max_y)
            max_y = array[i].getY();

    return max_y;
}

Point count_normal(const Point &p1, const Point &p2)
{
    return Point(p2.getY() - p1.getY(), p1.getX() - p2.getX());
}

int f_sign(int c)
{
    if (c < 0)
        return -1;
    else if (c > 0)
        return 1;
    else
        c = 0;

    return c;
}

bool Figure::is_convex()
{
    std::vector<Point> normals;

    for (const auto &edge : edge_list)
        normals.push_back(edge.get_normal());

    int prod_sign = f_sign(normals[0].vector_prod(normals[1]));

    for (int i = 1; i < normals.size() - 1; i++)
        if (prod_sign * f_sign(normals[i].vector_prod(normals[i + 1])) <= 0)
            return false;

    return true;
}

void Figure::count_normals()
{
    for (int i = 0; i < edge_list.size(); i++)
    {
        Point normal = count_normal(point_array[edge_list[i].getA()], point_array[edge_list[i].getB()]);
        if (normal * (point_array[edge_list[(i + 1) % edge_list.size()].getB()] - point_array[edge_list[i].getA()]) < 0)
        {
            normal.setX(-normal.getX());
            normal.setY(-normal.getY());
        }
        edge_list[i].set_normal(normal);
    }
}

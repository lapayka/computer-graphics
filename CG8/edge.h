#ifndef EDGE_H
#define EDGE_H

#include "point.h"


class Edge
{
    int A, B;
    Point normal;
    public:
        Edge();
        Edge(int a, int b);

        int getA() const
        {
            return A;
        }
        int getB() const
        {
            return B;
        }

        Point get_normal() const {return normal; };
        void set_normal(const Point &_normal) {normal = _normal; };
};

#endif // EDGE_H

#ifndef POINT_H
#define POINT_H


class Point
{
    int x, y;
    public:
        Point();
        Point(int x, int y)
        {
            this->x = x;
            this->y = y;
        }

        int getX() const
        {
            return x;
        }
        int getY() const
        {
            return y;
        }

        void setX(int _x)
        {
            x = _x;
        }

        void setY(int _y)
        {
            y = _y;
        }

        Point operator + (Point point) const {point.x = x + point.x; point.y = y + point.y; return point;}
        Point operator - (Point point) const {point.x = x - point.x; point.y = y - point.y; return point;}
        Point operator * (double num) const {Point buf(*this); buf.x *= num; buf.y *= num; return buf; }

        int operator * (const Point &point) const {return x * point.x + y * point.y; };
        int vector_prod (const Point &point) const {return x * point.y - y * point.x; };

        Point &operator = (const Point &_point) {x = _point.x; y = _point.y; return (*this); };
};

#endif // POINT_H

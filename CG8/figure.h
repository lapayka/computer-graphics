#ifndef FIGURE_H
#define FIGURE_H

#include <vector>
#include "point.h"
#include "edge.h"
#include <iostream>

class Figure
{
    std::vector<Point> point_array;
    std::vector<Edge> edge_list;

    bool end_of_input = true;
    int start_of_figure;

    public:
        Figure();

        void add_point(const Point &point);
        bool end_adding();
        void clear();
        bool is_convex();

        std::vector<Point> &getPoints()
        {
            return point_array;
        }
        std::vector<Edge> &getEdges()
        {
            return edge_list;
        }
    private:
        void count_normals();
};
#endif // FIGURE_H

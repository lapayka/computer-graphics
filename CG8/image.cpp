#include "image.h"

Image::Image(int x_start, int x_end, int width, int height, QWidget *parent) :
    QWidget(parent)
  , plast(0, 0)
{
    cutter_color = Qt::red;
    new_line_color = Qt::black;
    line_color =  Qt::green;


    cutter = Figure();
    image = new QImage(width, height, QImage::Format_RGB32);
    image->fill(Qt::white);
    setGeometry(x_start, x_end, width, height);

    setMouseTracking(true);
}

void Image::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(0, 0, *image);
}

int sign(int c)
{
    if (c < 0)
        return -1;
    else if (c > 0)
        return 1;
    else
        c = 0;

    return c;
}

void Image::drawLines(const std::vector<Line> &lines, const QColor &color)
{
    for (const auto &line : lines)
        drawLine(line, color);
    update();
}


void Image::inputPoint(const Point &point)
{
    if (points_count == 0)
    {
        plast = point;
        points_count++;
        return;
    }
    else
    {
        switch (input_style)
        {
        case DEFAULT_INPUT:
        {
            lines.push_back(Line(plast, point));
            drawLines(lines, line_color);
            //printf("%d %d\n", plast.getX(), plast.getY());
            break;
        }
        case HORIZONTAL_INPUT:
        {
            lines.push_back(Line(plast, Point((int)point.getX(), plast.getY())));
            drawLines(lines, line_color);
            break;
        }
        case VERTICAL_INPUT:
        {
            lines.push_back(Line(plast, Point(plast.getX(), (int)point.getY())));
            drawLines(lines, line_color);
            break;
        }
        }
        points_count = 0;
    }
    input_style = DEFAULT_INPUT;
    update();
}


void Image::inputRectPoint(const Point &point)
{
    switch (input_style)
    {
    case DEFAULT_INPUT:
    {
        plast = point;
        break;
    }
    case HORIZONTAL_INPUT:
    {
        plast = Point((int)point.getX(), plast.getY());
        break;
    }
    case VERTICAL_INPUT:
    {
        plast = Point(plast.getX(), (int)point.getY());
        break;
    }
    }

    cutter.add_point(plast);

    drawFigure();

    input_style = DEFAULT_INPUT;
    update();
}

void Image::mousePressEvent(QMouseEvent *event)
{
    printf("%d\n", input_type);
    switch (input_type)
    {
    case LINE_INPUT:
    {
        inputPoint(Point((int)(event->position().x()), (int)(event->position().y())));
        break;
    }
    case CUTTER_INPUT:
    {
        inputRectPoint(Point((int)(event->position().x()), (int)(event->position().y())));
    }
    }
}

void Image::clear()
{
    cutter.clear();
    lines.clear();
    image->fill(Qt::white);
    update();
}

void Image::drawFigure()
{
    std::vector<Point> &points = cutter.getPoints();
    std::vector<Edge> &edges = cutter.getEdges();

    for (int i = 0; i < edges.size(); i++)
    {
        drawLine(Point(points[edges[i].getA()].getX(), points[edges[i].getA()].getY()), Point(points[edges[i].getB()].getX(), points[edges[i].getB()].getY()), cutter_color);
    }
    update();
}

void Image::drawLine(const Point &pbegin, const Point &pend, const QColor &color)
{
    if (pbegin.getX() == pend.getX() && pbegin.getY() == pend.getY())
        {
            image->setPixelColor(pbegin.getX(), pbegin.getY(), color);
            return;
        }
        Point cur(pbegin);
        int dx, dy;
        bool fl = 0;
        int sx, sy, f;
        dx = pend.getX() - pbegin.getX();
        dy = pend.getY() - pbegin.getY();
        sx = sign(dx);
        sy = sign(dy);
        dx = abs(dx);
        dy = abs(dy);
        if (dy >= dx)
        {
            int tmp = dx;
            dx = dy;
            dy = tmp;
            fl = 1;
        }
        f = 2 * dy - dx;
        int len = dx;
        for (int i = 0; i <= len; i++)
        {
            //printf("**%d %d", cur.getX(), cur.getY());
            image->setPixelColor(cur.getX(), cur.getY(), color);
            if (f >= 0)
            {
                if (fl == 1)
                    cur.setX(cur.getX() + sx);
                else
                    cur.setY(cur.getY() + sy);
                f -= 2 * dx;
            }
            if (f < 0)
            {
                if (fl == 1)
                    cur.setY(cur.getY() + sy);
                else
                    cur.setX(cur.getX() + sx);
            }
            f += 2 * dy;
        }
}


void Image::cut()
{
    std::vector<Point> cut_points = cutter.getPoints();



    for (const auto &line : lines)
    {
        Point p1 = line.get_fst();
        Point p2 = line.get_scd();

        Point D = p2 - p1;

        double ts = 0, te = 1;
        bool visible = true;

        for (const auto &edge : cutter.getEdges())
        {
            Point f = cutter.getPoints()[edge.getA()];
            Point n = edge.get_normal();
            //printf("s: %d %d\n", f.getX(), f.getY());
            //printf("e: %d %d\n", cutter.getPoints()[edge.getB()].getX(), cutter.getPoints()[edge.getB()].getY());
            //printf("n: %d %d\n", n.getX(), n.getY());

            Point w = p1 - f;

            int w_prod = n * w;
            int d_prod = n * D;

            if (d_prod != 0)
            {
                double t = -(double)w_prod / d_prod;

                if (d_prod > 0)
                {
                    if (t > 1.0)
                    {
                        visible = false;
                        break;
                    }
                    ts = ts > t ? ts : t;
                }
                if (d_prod < 0)
                {
                    if (t < 0.0)
                    {
                        visible = false;
                        break;
                    }
                    te = te < t ? te : t;
                }
            }
            else
            {
                if (w_prod < 0)
                    visible = false;
            }
        }

        std::cout << ts << " " << te << "\n";
        if (ts < te && visible)
            drawLine(p1 + (p2 - p1) * ts, p1 + (p2 - p1) * te, new_line_color);
    }
    update();
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QPainter>

#include "image.h"

#include "figure.h"

#define WINDOW_HEIGHT 500
#define WINDOW_WIDTH 500

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();
private slots:
        void on_pushButton_clicked();

        void on_pushButton_2_clicked();

        void on_pushButton_3_clicked();

        void on_pushButton_4_clicked();

        void on_pushButton_5_clicked();

        void on_pushButton_6_clicked();

        void on_Yellow_clicked();

        void on_Red_clicked();

        void on_Blue_clicked();

        void on_Green_clicked();

        void on_Black_clicked();

        void on_Yellow_2_clicked();

        void on_Red_2_clicked();

        void on_Blue_2_clicked();

        void on_Green_2_clicked();

        void on_Black_2_clicked();

private:
        Ui::MainWindow *ui;
        bool input_figure;
        Image *image;
};
#endif // MAINWINDOW_H

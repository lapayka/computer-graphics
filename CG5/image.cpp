#include "image.h"

Image::Image(int x_start, int x_end, int width, int height, QWidget *parent) :
    QWidget(parent)
  , plast(0, 0)
{
    pen_color = Qt::red;
    brush_color = Qt::green;
    figure = Figure();
    image = new QImage(width, height, QImage::Format_RGB32);
    bg_color = Qt::white;
    image->fill(bg_color);
    setGeometry(x_start, x_end, width, height);

    setMouseTracking(true);
}

void Image::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(0, 0, *image);
}

int sign(int c)
{
    if (c < 0)
        return -1;
    else if (c > 0)
        return 1;
    else
        c = 0;

    return c;
}

void Image::mousePressEvent(QMouseEvent *event)
{
    printf("%d\n", input_type);
    switch (input_type)
    {
        case DEFAULT_INPUT:
        {
            plast = Point((int)(event->position().x()), (int)(event->position().y()));
            printf("%d %d\n", plast.getX(), plast.getY());
            break;
        }
        case HORIZONTAL_INPUT:
        {
            plast = Point((int)(event->position().x()), plast.getY());
            break;
        }
        case VERTICAL_INPUT:
        {
            plast = Point(plast.getX(), (int)(event->position().y()));
            break;
        }
    }
    repaint();
    input_type = DEFAULT_INPUT;
    figure.add_point(plast);
    drawFigure();
}

void Image::clear()
{
    figure.clear();
    image->fill(bg_color);
    update();
}

void Image::drawFigure()
{
    std::vector<Point> &points = figure.getPoints();
    std::vector<Edge> &edges = figure.getEdges();

    for (int i = 0; i < edges.size(); i++)
    {
        drawLine(Point(points[edges[i].getA()].getX(), points[edges[i].getA()].getY()), Point(points[edges[i].getB()].getX(), points[edges[i].getB()].getY()));
    }
    update();
}

void Image::drawLine(const Point &pbegin, const Point &pend)
{
    if (pbegin.getX() == pend.getX() && pbegin.getY() == pend.getY())
        {
            image->setPixelColor(pbegin.getX(), pbegin.getY(), pen_color);
            return;
        }
        Point cur(pbegin);
        int dx, dy;
        bool fl = 0;
        int sx, sy, f;
        dx = pend.getX() - pbegin.getX();
        dy = pend.getY() - pbegin.getY();
        sx = sign(dx);
        sy = sign(dy);
        dx = abs(dx);
        dy = abs(dy);
        if (dy >= dx)
        {
            int tmp = dx;
            dx = dy;
            dy = tmp;
            fl = 1;
        }
        f = 2 * dy - dx;
        int len = dx;
        for (int i = 0; i <= len; i++)
        {
            //printf("**%d %d", cur.getX(), cur.getY());
            image->setPixelColor(cur.getX(), cur.getY(), pen_color);
            if (f >= 0)
            {
                if (fl == 1)
                    cur.setX(cur.getX() + sx);
                else
                    cur.setY(cur.getY() + sy);
                f -= 2 * dx;
            }
            if (f < 0)
            {
                if (fl == 1)
                    cur.setY(cur.getY() + sy);
                else
                    cur.setX(cur.getX() + sx);
            }
            f += 2 * dy;
        }
}

void sleepFeature()
{
    QTime end = QTime::currentTime().addMSecs(10);
    while (QTime::currentTime() < end)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 1);
    }
    return;
}

int max_x(std::vector<Point> &array)
{
    int max_x = array[0].getX();
    for (int i = 1; i < array.size(); i++)
        if (array[i].getX() > max_x)
            max_x = array[i].getX();

    return max_x;
}

int max_y(std::vector<Point> &array)
{
    int max_y = array[0].getY();
    for (int i = 1; i < array.size(); i++)
        if (array[i].getY() > max_y)
            max_y = array[i].getY();


    return max_y;
}

int min_x(std::vector<Point> &array)
{
    int min_x = array[0].getX();
    for (int i = 1; i < array.size(); i++)
        if (array[i].getX() < min_x)
            min_x = array[i].getX();

    return min_x;
}
int min_y(std::vector<Point> &array)
{
    int min_y = array[0].getY();
    for (int i = 1; i < array.size(); i++)
        if (array[i].getY() < min_y)
            min_y = array[i].getY();


    return min_y;
}

static inline void swap(int &a, int &b)
{
    int tmp = a;
    a = b;
    b = tmp;
}

void Image::invert(QColor &color)
{
    if (color == brush_color)
        color = bg_color;
    else
        color = brush_color;
}

void Image::shade_string(int y, int x_start, int x_end)
{
    for (; x_start < x_end; x_start++)
    {
        QColor color = image->pixelColor(x_start, y);
        invert(color);
        image->setPixelColor(x_start, y, color);
    }

}

void Image::fill(bool delay)
{
    std::vector<Point> point_array(figure.getPoints());
    std::vector<Edge> edge_list(figure.getEdges());

    int right_x = max_x(point_array);
    int high_y = max_y(point_array);

    int left_x = min_x(point_array);
    int bottom_y = min_y(point_array);

    printf("%d %d %d %d\n", right_x,  left_x, bottom_y, high_y);

    int partition = (right_x + left_x) / 2;

    for(int i = 0; i < edge_list.size(); i++)
    {
        if (point_array[edge_list[i].getA()].getY() == point_array[edge_list[i].getB()].getY())
            continue;

        int x_start = point_array[edge_list[i].getA()].getX();
        int y_start = point_array[edge_list[i].getA()].getY();
        int x_end = point_array[edge_list[i].getB()].getX();
        int y_end = point_array[edge_list[i].getB()].getY();


        int max = y_end;
        int min = y_start;

        if (min > max)
        {
            swap(min, max);
            swap(x_end, x_start);
        }


        int lx = (x_end - x_start);
        int ly = max - min;

        double dx;
        dx = (double)lx / ly;
        int dy = 1;

        double x = x_start;
        int y = min;

        for (int i = 0; i < abs(ly); i++)
        {
            //printf(" %d %d %d %d %d\n",y,  max, min, y_start, y_end);
            if (x < partition)
                shade_string(y, round(x), partition);
            else
                shade_string(y, partition, round(x));

            x += dx;
            y += dy;

            if (delay)
            {
                update();
                sleepFeature();
            }
        }
    }
    drawFigure();
    update();
}


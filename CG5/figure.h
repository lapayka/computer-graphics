#ifndef FIGURE_H
#define FIGURE_H

#include <vector>
#include "point.h"
#include "edge.h"
#include <iostream>

class Figure
{
    std::vector<Point> point_array;
    std::vector<Edge> edge_list;

    bool end_of_input = true;
    int start_of_figure;

    public:
        Figure();

        void add_point(Point &point);
        void end_adding();
        void clear();

        std::vector<Point> &getPoints()
        {
            return point_array;
        }
        std::vector<Edge> &getEdges()
        {
            return edge_list;
        }
};
#endif // FIGURE_H

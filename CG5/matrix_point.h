#ifndef MATRIX_POINT_H
#define MATRIX_POINT_H

#include <QColor>

class Matrix_point
{
    int x_start, y_start;
    int n, m;
    std::vector<QColor> *matrix;

    public:
        void setXStart(int x)
        {
            x_start = x;
        }
        void setYStart(int y)
        {
            y_start = y;
        }

        Matrix_point();
        void allocate(int row_count, int col_count);
        void shade_string(int row, int col_start, int col_end);

        std::vector<QColor> *getMatrix()
        {
            return matrix;
        }

        int getRowCount()
        {
            return n;
        }
        int getColCount()
        {
            return m;
        }
        int getXStart()
        {
            return x_start;
        }
        int getYStart()
        {
            return y_start;
        }
};

#endif // MATRIX_POINT_H

#include "matrix_point.h"

Matrix_point::Matrix_point()
{

}

void Matrix_point::allocate(int row_count, int col_count)
{
    //delete matrix;
    n = row_count;
    m = col_count;

    matrix = new std::vector<QColor>[row_count];

    for (int i = 0; i < row_count; i++)
        for(int j = 0; j < col_count; j++)
            matrix[i].push_back(Qt::white);
}

static inline void invert(QColor& color)
{
    int r, g, b;

    color.getRgb(&r, &g, &b);

    r = 255 - r;
    g = 255 - g;
    b = 255 - b;

    color.setRgb(r, g, b);
}

void Matrix_point::shade_string(int row, int col_start, int col_end)
{
    for (int i = col_start; i < col_end; i++)
    {
        //printf("%d %d %d %d %d\n", row, col_start, col_end, n, m);
        invert(matrix[row][i]);
    }
}

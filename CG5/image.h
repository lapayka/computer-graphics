#ifndef IMAGE_H
#define IMAGE_H

#include <QWidget>
#include <QImage>
#include <QPainter>
#include <QMouseEvent>
#include <QPaintEvent>
#include "figure.h"
#include <stack>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <QTime>
#include <QCoreApplication>

#define DEFAULT_INPUT 0
#define HORIZONTAL_INPUT 1
#define VERTICAL_INPUT 2
#define SEED_INPUT 4

#include <QPainter>
#include <iostream>

class Image : public QWidget
{
    Q_OBJECT
public:
    explicit Image(int x_start, int x_end, int width, int height, QWidget *parent);
    void mousePressEvent(QMouseEvent *event);
    void fill(bool Delay);
    void EndFigureInput(){figure.end_adding(); drawFigure(); };
    void SetInputType(int type) {input_type = type; };
    void SetBrushColor(const QColor &color) {brush_color = color; };
    void SetPenColor(const QColor &color) {pen_color = color; };
    void SetDelay(bool _delay) {delay = _delay; };
    void drawLine(const Point &pbegin, const Point &pend);
    void drawFigure();
    void clear();
    void paintEvent(QPaintEvent *event);
    void shade_string(int y, int x_start, int x_end);
    void invert(QColor &color);

private:
    QImage *image;
    Figure figure;

    QColor brush_color;
    QColor pen_color;
    QColor bg_color;

    int input_type = DEFAULT_INPUT;
    Point plast;

    bool delay;

signals:

};

#endif // IMAGE_H

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    image = new Image(0, 0, 600, 600, this);
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    image->EndFigureInput();
}

void MainWindow::on_pushButton_2_clicked()
{
    image->SetInputType(HORIZONTAL_INPUT);
}

void MainWindow::on_pushButton_6_clicked()
{
    image->SetInputType(VERTICAL_INPUT);
}

void MainWindow::on_pushButton_3_clicked()
{
    input_figure = false;
    image->fill(false);
}

void MainWindow::on_pushButton_4_clicked()
{
    input_figure = false;
    image->fill(true);
}

void MainWindow::on_pushButton_5_clicked()
{
    image->clear();
}

void MainWindow::on_Yellow_clicked()
{
    image->SetBrushColor(Qt::yellow);
}

void MainWindow::on_Red_clicked()
{
    image->SetBrushColor(Qt::red);
}

void MainWindow::on_Blue_clicked()
{
    image->SetBrushColor(Qt::blue);
}

void MainWindow::on_Green_clicked()
{
    image->SetBrushColor(Qt::green);
}

void MainWindow::on_Black_clicked()
{
    image->SetBrushColor(Qt::black);
}

void MainWindow::on_Yellow_2_clicked()
{
    image->SetPenColor(Qt::yellow);
}

void MainWindow::on_Red_2_clicked()
{
    image->SetPenColor(Qt::red);
}

void MainWindow::on_Blue_2_clicked()
{
    image->SetPenColor(Qt::blue);
}

void MainWindow::on_Green_2_clicked()
{
    image->SetPenColor(Qt::green);
}

void MainWindow::on_Black_2_clicked()
{
    image->SetPenColor(Qt::black);
}

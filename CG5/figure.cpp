#include "figure.h"
#include <windows.h>

Figure::Figure()
{
    end_of_input = true;
}

void Figure::clear()
{
    point_array.clear();
    edge_list.clear();

    end_of_input = true;
}

void Figure::add_point(Point &point)
{
    point_array.push_back(point);


    if (end_of_input)
    {
        start_of_figure = point_array.size() - 1;
        end_of_input = false;
        return;
    }

    Edge edge(point_array.size() - 1, point_array.size() - 2);
    edge_list.push_back(edge);
}

void Figure::end_adding()
{
    Edge edge(point_array.size() - 1, start_of_figure);
    edge_list.push_back(edge);
    end_of_input = true;
}

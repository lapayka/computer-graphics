#include "painterform.h"
#include "ui_painterform.h"

#define TABULATION 15
#define INT_EPS 3

PainterForm::PainterForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PainterForm)
{
    ui->setupUi(this);
}

PainterForm::~PainterForm()
{
    delete ui;
}


extern scr_triangle_t scr_triangle;
extern scr_ellipse_t scr_ellipse;
extern axis_t axis;
extern triangle_t result;
extern bool Draw;

void PainterForm::draw_line(scr_pt_t &A, scr_pt_t &B, QPainter &painter)
{
    painter.drawLine(A.x, A.y, B.x, B.y);
}

void PainterForm::draw_scr_triangle(scr_triangle_t &tr, QPainter &painter)
{
    draw_line(tr.A, tr.B, painter);
    draw_line(tr.C, tr.B, painter);
    draw_line(tr.A, tr.C, painter);
}

void PainterForm::paint()
{
    repaint();
}

void PainterForm::paint_arrow(axis_t &axis, QPainter &painter)
{
    painter.drawLine(axis.x, 0, axis.x - 10, 10);
    painter.drawLine(axis.x, 0, axis.x + 10, 10);
    painter.drawLine(WINDOW_WIDTH, axis.y, WINDOW_WIDTH - 10, axis.y + 10);
    painter.drawLine(WINDOW_WIDTH, axis.y, WINDOW_WIDTH - 10, axis.y - 10);
}

void PainterForm::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
    painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
    painter.drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    QPoint center;

    center.setX(scr_ellipse.center.x);
    center.setY(scr_ellipse.center.y);
    //painter.setBrush(QBrush(Qt::NoBrush, Qt::SolidPattern));
    painter.drawEllipse(center, scr_ellipse.rx, scr_ellipse.ry);

    draw_scr_triangle(scr_triangle, painter);

    scr_pt_t A = scr_triangle.A;
    scr_pt_t B = scr_triangle.B;
    scr_pt_t C = scr_triangle.C;

    A.x += (A.x + INT_EPS >= scr_triangle.C.x && A.x + INT_EPS >= scr_triangle.B.x) ? TABULATION : 0;
    A.x += (A.x - INT_EPS <= scr_triangle.C.x && A.x - INT_EPS <= scr_triangle.B.x) ? -TABULATION * 2 : 0;
    A.y += (A.y - INT_EPS <= scr_triangle.C.y && A.y - INT_EPS <= scr_triangle.B.y) ? -TABULATION : TABULATION;
    B.x += (B.x + INT_EPS >= scr_triangle.C.x && B.x + INT_EPS >= scr_triangle.A.x) ? TABULATION : 0;
    B.x += (B.x - INT_EPS <= scr_triangle.C.x && B.x - INT_EPS <= scr_triangle.A.x) ? -TABULATION * 2 : 0;
    B.y += (B.y - INT_EPS <= scr_triangle.C.y && B.y - INT_EPS <= scr_triangle.A.y) ? -TABULATION : TABULATION;
    C.x += (C.x + INT_EPS >= scr_triangle.B.x && C.x + INT_EPS >= scr_triangle.A.x) ? TABULATION : 0;
    C.x += (C.x - INT_EPS <= scr_triangle.B.x && C.x - INT_EPS <= scr_triangle.A.x) ? -TABULATION * 2 : 0;
    A.y += (C.y - INT_EPS <= scr_triangle.B.y && C.y - INT_EPS <= scr_triangle.A.y) ? -TABULATION : TABULATION;
    if (Draw)
    {
        painter.drawText(A.x, A.y, QString("%1(%2,%3)").arg(result.A.number).arg(result.A.x).arg(result.A.y));
        painter.drawText(B.x, B.y, QString("%1(%2,%3)").arg(result.B.number).arg(result.B.x).arg(result.B.y));
        painter.drawText(C.x, C.y, QString("%1(%2,%3)").arg(result.C.number).arg(result.C.x).arg(result.C.y));
        painter.drawText(axis.x + 10, axis.y -20, QString("O(%1,%2)").arg(0).arg(0));


        painter.drawLine(0, axis.y, WINDOW_WIDTH, axis.y);
        painter.drawLine(axis.x, 0, axis.x, WINDOW_HEIGHT);
        paint_arrow(axis, painter);
    }
}

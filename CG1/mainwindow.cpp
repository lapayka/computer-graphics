#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Form = new PainterForm;
    Form->show();
    ui->Condition_label->setHidden(true);
    connect(this, SIGNAL(RepaintSignal()), Form, SLOT(paint()), Qt::DirectConnection);
}


scr_triangle_t scr_triangle;
scr_ellipse_t scr_ellipse;
axis_t axis;
triangle_t result;

bool Draw = 0;

MainWindow::~MainWindow()
{
    Form->~PainterForm();
    delete ui;
}

int MainWindow::read_pt_arr(point_t arr[], int &n)
{
    bool flag;
    n = 0;
    for (int i = 0; i < ROW_COUNT; i++)
    {
        QTableWidgetItem *item = ui->tableWidget->item(i, 0);
        if (!item)
            break;
        arr[n].x = item->text().toDouble(&flag);
        if (!flag)
        {
            if (item->text() == "")
                continue;
            else
                return EXIT_FAILURE;
        }

        item = ui->tableWidget->item(i, 1);
        if (!item)
            return EXIT_FAILURE;

        arr[n].y = item->text().toDouble(&flag);
        if (!flag)
            return EXIT_FAILURE;
        arr[n++].number = i + 1;
    }

    return EXIT_SUCCESS;
}

double tr_square(triangle_t &triangle)
{
    double p = (triangle.a + triangle.b + triangle.c) / 2.0;

    return sqrt(p * (p - triangle.a) * (p - triangle.b) * (p - triangle.c));
}

double circ_square(triangle_t &triangle)
{
    double S = tr_square(triangle);

    return M_PI * ((triangle.a * triangle.b * triangle.c) / (4.0 * S)) * ((triangle.a * triangle.b * triangle.c) / (4.0 * S));
}

void create_triangle(point_t A, point_t B, point_t C, triangle_t &triangle)
{
    triangle.A = A;
    triangle.B = B;
    triangle.C = C;

    triangle.a = sqrt((A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y));
    triangle.b = sqrt((A.x - C.x) * (A.x - C.x) + (A.y - C.y) * (A.y - C.y));
    triangle.c = sqrt((C.x - B.x) * (C.x - B.x) + (C.y - B.y) * (C.y - B.y));
}

void MainWindow::set_result_table(triangle_t &triangle)
{
    QTableWidgetItem *item = ui->Result_TW->item(0, 0);
    item->setText(QString::number(triangle.A.number, 10));

    item = ui->Result_TW->item(0, 1);
    item->setText(QString::number(triangle.A.x, 'g', 3));

    item = ui->Result_TW->item(0, 2);
    item->setText(QString::number(triangle.A.y, 'g', 3));

    item = ui->Result_TW->item(1, 0);
    item->setText(QString::number(triangle.B.number, 10));

    item = ui->Result_TW->item(1, 1);
    item->setText(QString::number(triangle.B.x, 'g', 3));

    item = ui->Result_TW->item(1, 2);
    item->setText(QString::number(triangle.B.y, 'g', 3));

    item = ui->Result_TW->item(2, 0);
    item->setText(QString::number(triangle.C.number, 10));

    item = ui->Result_TW->item(2, 1);
    item->setText(QString::number(triangle.C.x, 'g', 3));

    item = ui->Result_TW->item(2, 2);
    item->setText(QString::number(triangle.C.y, 'g', 3));
}

int find_triangle(point_t pt_arr[], int n, triangle_t &result)
{
    double min_square_dif = 0.0;

    for (int i = 0; i < n - 2; i++)
        for (int j = i + 1; j < n - 1; j++)
            for (int z = i + 2; z < n; z++)
            {
                triangle_t cur_triangle;
                create_triangle(pt_arr[i], pt_arr[j], pt_arr[z], cur_triangle);
                double triangle_square = tr_square(cur_triangle);
                if (fabs(triangle_square) < EPS || isnan(triangle_square))
                    continue;

                double circle_square = circ_square(cur_triangle);

                if ((fabs(min_square_dif) < EPS) || (circle_square - triangle_square < min_square_dif))
                {
                    min_square_dif = circle_square - triangle_square;
                    result = cur_triangle;
                }
            }

    if (fabs(min_square_dif) < EPS || isnan(min_square_dif))
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}


double min(size_t n, ...)
{
    va_list v1;

    va_start(v1, n);

    double min = va_arg(v1, double);
    double cur;
    for (size_t i = 1; i < n; i++)
        if ((cur = va_arg(v1, double)) < min)
            min = cur;

    va_end(v1);

    return min;
}

double max(size_t n, ...)
{
    va_list v1;

    va_start(v1, n);

    double max = va_arg(v1, double);
    double cur;
    for (size_t i = 1; i < n; i++)
        if ((cur = va_arg(v1, double)) > max)
            max = cur;

    va_end(v1);

    return max;
}


void scale_point(point_t &src, double kx, double ky, scr_pt_t &dst)
{
    dst.x = round(kx * (src.x)) + axis.x;
    dst.y = axis.y - round(ky * (src.y));
}

void get_scr_coordinates(triangle_t &triangle, ellipse_t &ellipse, scr_triangle_t &scr_triangle, scr_ellipse_t &scr_ellipse)
{
    double min_x = ellipse.center.x - ellipse.rx;
    double max_x = ellipse.center.x + ellipse.rx;
    double min_y = ellipse.center.y - ellipse.ry;
    double max_y = ellipse.center.y + ellipse.ry;

    double kx = (max_x * min_x < 0.0) ? ((WINDOW_WIDTH - WINDOW_BORDER * 2) / (max_x - min_x)) : ((WINDOW_WIDTH - WINDOW_BORDER * 2) / max(2, fabs(min_x), fabs(max_x)));
    double ky = (max_y * min_y < 0.0) ? ((WINDOW_HEIGHT - WINDOW_BORDER * 2) / (max_y - min_y)) : ((WINDOW_HEIGHT - WINDOW_BORDER * 2) / max(2, fabs(min_y), fabs(max_y)));

    kx = ky = min(2, kx, ky);

    scale_point(triangle.A, kx, ky, scr_triangle.A);
    scale_point(triangle.B, kx, ky, scr_triangle.B);
    scale_point(triangle.C, kx, ky, scr_triangle.C);
    scale_point(ellipse.center, kx, ky, scr_ellipse.center);

    scr_pt_t A = scr_triangle.A;
    scr_pt_t B = scr_triangle.B;
    scr_pt_t C = scr_triangle.C;

    double a = sqrt((A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y));
    double b = sqrt((C.x - B.x) * (C.x - B.x) + (C.y - B.y) * (C.y - B.y));
    double c = sqrt((A.x - C.x) * (A.x - C.x) + (A.y - C.y) * (A.y - C.y));

    double p = (a + b + c) / 2.0;

    scr_ellipse.rx = scr_ellipse.ry = round((a * b * c) / (4.0 * sqrt(p * (p - a) * (p - b) * (p - c))));

    //scr_ellipse.rx = round(ellipse.rx * (kx));
   // scr_ellipse.ry = round(ellipse.ry * (ky));
}

void get_scr_Axis(ellipse_t &ellipse)
{
    double min_x = ellipse.center.x - ellipse.rx;
    double max_x = ellipse.center.x + ellipse.rx;
    double min_y = ellipse.center.y - ellipse.ry;
    double max_y = ellipse.center.y + ellipse.ry;


    if (min_x * max_x < 0.0)
        axis.x = (WINDOW_WIDTH - 2 * WINDOW_BORDER) * (fabs(min_x) / (max_x - min_x)) + WINDOW_BORDER;
    else
    {
        if (max_x < 0.0)
            axis.x = WINDOW_WIDTH - WINDOW_BORDER;
        else
            axis.x = WINDOW_BORDER;
    }

    if (min_y * max_y < 0.0)
        axis.y = WINDOW_HEIGHT - WINDOW_BORDER - (WINDOW_HEIGHT - 2 * WINDOW_BORDER) * (fabs(min_y) / (max_y - min_y));
    else
    {
        if (max_y > 0.0)
            axis.y = WINDOW_HEIGHT - WINDOW_BORDER;
        else
            axis.y = WINDOW_BORDER;
    }
}

void calc_ellipse_params(triangle_t &triangle, ellipse_t &ellipse)
{
    point_t A = triangle.A;
    point_t B = triangle.B;
    point_t C = triangle.C;

    double first = (B.x * B.x + B.y * B.y - C.x * C.x - C.y * C.y);
    double second = (C.x * C.x + C.y * C.y - A.x * A.x - A.y * A.y);
    double third = (A.x * A.x + A.y * A.y - B.x * B.x - B.y * B.y);
    double denominator = A.x * (B.y - C.y) + B.x * (C.y - A.y) + C.x * (A.y - B.y);

    ellipse.center.x = -(A.y * first + B.y * second + C.y * third) / denominator / 2;
    ellipse.center.y = (A.x * first + B.x * second + C.x * third) / denominator / 2;

    double a = sqrt((A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y));
    double b = sqrt((C.x - B.x) * (C.x - B.x) + (C.y - B.y) * (C.y - B.y));
    double c = sqrt((A.x - C.x) * (A.x - C.x) + (A.y - C.y) * (A.y - C.y));

    double p = (a + b + c) / 2.0;

    ellipse.rx = ellipse.ry = round((a * b * c) / (4.0 * sqrt(p * (p - a) * (p - b) * (p - c))));
}


void MainWindow::on_DrawPB_clicked()
{
    Draw = true;
    ui->ErrorLE->setText("");
    memset(&scr_triangle, 0 , sizeof(scr_triangle_t));
    memset(&scr_ellipse, 0 , sizeof(scr_ellipse_t));

    point_t pt_arr[ui->tableWidget->rowCount()];

    int n = 0;
    if (read_pt_arr(pt_arr, n) != EXIT_SUCCESS)
    {
       ui->ErrorLE->setText("Неправильный ввод");
       return;
    }

    if (n <= 2)
    {
        ui->ErrorLE->setText("Введено меньше 3х точек");
        return;
    }

    if (find_triangle(pt_arr, n, result) != EXIT_SUCCESS)
    {
        ui->ErrorLE->setText("Все трегольники вырожденные");
        return;
    }

    set_result_table(result);

    ellipse_t ellipse;

    calc_ellipse_params(result, ellipse);
    get_scr_Axis(ellipse);
    get_scr_coordinates(result, ellipse, scr_triangle, scr_ellipse);

    emit RepaintSignal();
}

void MainWindow::on_pushButton_clicked()
{
    ui->Condition_label->setHidden(false);
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->Condition_label->setHidden(true);
}

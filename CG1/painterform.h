#ifndef PAINTERFORM_H
#define PAINTERFORM_H

#include <QWidget>
#include <QPainter>
#include <QListView>

typedef struct
{
    double x;
    double y;

    int number;
} point_t;

typedef struct
{
    point_t A;
    point_t B;
    point_t C;

    double a;
    double b;
    double c;

    int pt_numbers[3];
} triangle_t;

typedef struct
{
    int x;
    int y;
} scr_pt_t;

typedef struct
{
    scr_pt_t A;
    scr_pt_t B;
    scr_pt_t C;
} scr_triangle_t;

typedef struct
{
    int x;
    int y;
} axis_t;

typedef struct
{
    point_t center;
    double rx;
    double ry;
} ellipse_t;

typedef struct
{
    scr_pt_t center;
    int rx;
    int ry;
} scr_ellipse_t;

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 800
#define WINDOW_BORDER 50
#define EPS 1e-3
#define ROW_COUNT 10

namespace Ui {
class PainterForm;
}

class PainterForm : public QWidget
{
    Q_OBJECT

public:
    explicit PainterForm(QWidget *parent = nullptr);
    void draw_line(scr_pt_t &A, scr_pt_t &B, QPainter &painter);
    void draw_scr_triangle(scr_triangle_t &tr, QPainter &painter);
    void paintEvent(QPaintEvent *event);
    void paint_arrow(axis_t &axis, QPainter &painter);
    ~PainterForm();

private slots:
    void paint();

private:
    Ui::PainterForm *ui;
};

#endif // PAINTERFORM_H

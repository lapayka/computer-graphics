#include "image.h"

Image::Image(int x_start, int x_end, int width, int height, QWidget *parent) :
    QWidget(parent)
  , plast(0, 0)
  , cutter()
  , figure()
{
    cutter_color = Qt::red;
    new_line_color = Qt::black;
    line_color =  Qt::green;

    image = new QImage(width, height, QImage::Format_RGB32);
    image->fill(Qt::white);
    setGeometry(x_start, x_end, width, height);

    setMouseTracking(true);
}

void Image::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(0, 0, *image);
}

int sign(int c)
{
    if (c < 0)
        return -1;
    else if (c > 0)
        return 1;
    else
        c = 0;

    return c;
}




void Image::inputRectPoint(Figure &object, const Point &point)
{
    switch (input_style)
    {
    case DEFAULT_INPUT:
    {
        plast = point;
        break;
    }
    case HORIZONTAL_INPUT:
    {
        plast = Point((int)point.getX(), plast.getY());
        break;
    }
    case VERTICAL_INPUT:
    {
        plast = Point(plast.getX(), (int)point.getY());
        break;
    }
    }

    object.add_point(plast);
    input_style = DEFAULT_INPUT;
}

void Image::mousePressEvent(QMouseEvent *event)
{
    printf("%d\n", input_type);
    switch (input_type)
    {
    case FIGURE_INPUT:
    {
        inputRectPoint(figure, Point((int)(event->position().x()), (int)(event->position().y())));
        drawFigure(figure, line_color);

        break;
    }
    case CUTTER_INPUT:
    {
        inputRectPoint(cutter, Point((int)(event->position().x()), (int)(event->position().y())));
        drawFigure(cutter, cutter_color);
    }
    }
    update();
}

void Image::clear()
{
    cutter.clear();
    figure.clear();
    image->fill(Qt::white);
    update();
}

void Image::drawFigure(Figure &figure, const QColor &color)
{
    std::vector<Point> &points = figure.getPoints();
    std::vector<Edge> &edges = figure.getEdges();

    for (int i = 0; i < edges.size(); i++)
    {
        drawLine(points[i], points[i + 1], color);
    }
    update();
}

void Image::drawLine(const Point &pbegin, const Point &pend, const QColor &color)
{
    if (pbegin.getX() == pend.getX() && pbegin.getY() == pend.getY())
        {
            image->setPixelColor(pbegin.getX(), pbegin.getY(), color);
            return;
        }
        Point cur(pbegin);
        int dx, dy;
        bool fl = 0;
        int sx, sy, f;
        dx = pend.getX() - pbegin.getX();
        dy = pend.getY() - pbegin.getY();
        sx = sign(dx);
        sy = sign(dy);
        dx = abs(dx);
        dy = abs(dy);
        if (dy >= dx)
        {
            int tmp = dx;
            dx = dy;
            dy = tmp;
            fl = 1;
        }
        f = 2 * dy - dx;
        int len = dx;
        for (int i = 0; i <= len; i++)
        {
            //printf("**%d %d", cur.getX(), cur.getY());
            image->setPixelColor(cur.getX(), cur.getY(), color);
            if (f >= 0)
            {
                if (fl == 1)
                    cur.setX(cur.getX() + sx);
                else
                    cur.setY(cur.getY() + sy);
                f -= 2 * dx;
            }
            if (f < 0)
            {
                if (fl == 1)
                    cur.setY(cur.getY() + sy);
                else
                    cur.setX(cur.getX() + sx);
            }
            f += 2 * dy;
        }
}


void sleepFeature()
{
    QTime end = QTime::currentTime().addMSecs(10000);
    while (QTime::currentTime() < end)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 1);
    }
    return;
}

void Image::cut()
{
    std::vector<Point> &cut_points = cutter.getPoints();

    QColor colors[4] = {Qt::yellow, Qt::green, Qt::blue, Qt::black};

    for (int i = 0; i < cut_points.size() - 1; i++)
    {
        Figure result;
        Point S;
        std::vector<Point> &fig_points =figure.getPoints();

        Point n = cutter.getEdges()[i].get_normal();

        for (int j = 0; j < fig_points.size(); j++)
        {
            if (j != 0)
            {
                Point w = S - cut_points[i];
                Point d = fig_points[j] - S;

                int w_prod = n * w;
                int d_prod = n * d;

                if (d_prod != 0)
                {
                    double t = -(double)w_prod / d_prod;

                    if (t <= 1.0 && t >= 0.0)
                        result.add_point(S + d * t);
                }
            }
            S = fig_points[j];

            if ((S - cut_points[i]) * n > 0)
                result.add_point(S);

        }
        result.end_adding();
        figure = result;
    }
    drawFigure(figure, new_line_color);
}

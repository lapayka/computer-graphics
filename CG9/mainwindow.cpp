#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    image = new Image(0, 0, 600, 600, this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    image->SetInputStyle(HORIZONTAL_INPUT);
}

void MainWindow::on_pushButton_2_clicked()
{
    image->SetInputStyle(VERTICAL_INPUT);
}

void MainWindow::on_pushButton_7_clicked()
{
    image->EndFigureInput();
}

void MainWindow::on_pushButton_3_clicked()
{
    image->SetInputType(CUTTER_INPUT);
}

void MainWindow::on_pushButton_5_clicked()
{
    if (!image->EndCutterInput())
        ui->lineEdit->setText("Неправильный многоугольник");
    else
        ui->lineEdit->setText("");
}



void MainWindow::on_pushButton_4_clicked()
{
    image->cut();
}

void MainWindow::on_pushButton_6_clicked()
{
    image->clear();
}
